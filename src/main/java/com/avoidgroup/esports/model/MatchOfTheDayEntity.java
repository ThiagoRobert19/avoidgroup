package com.avoidgroup.esports.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.stereotype.Component;

import com.avoidgroup.util.EntidadeBase;

@Component("MatchOfTheDayEntity")
@Entity
public class MatchOfTheDayEntity implements Serializable, EntidadeBase, Comparable<MatchOfTheDayEntity> {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String dateUpdate;

	private String begin_at;
	private String begin_atConverted;

	private String beginHour;
	private String match_type;
	private String number_of_games;

	private String serie_id;
	private String serie_prize;
	private String serie_year;
	private String serie_begin;
	private String serie_season;

	public String getSerie_id() {
		return serie_id;
	}

	public void setSerie_id(String serie_id) {
		this.serie_id = serie_id;
	}

	private String serie_description;
	private String serie_end_at;
	private String serie_full_name;
	private String serie_name;

	private String tournament_begin_at;
	private String tournament_end_at;
	private String tournament_id;
	private String tournament_name;

	private String winner;
	private String winner_id;

	private String videogame;
	private String videogame_img;

	private String opponent_type;

	private String opponent1_id;
	private String opponent1_name;
	private String opponent1_image_url;

	private String opponent2_id;
	private String opponent2_name;
	private String opponent2_image_url;

	private String results_score1;
	private String results_team_id1;

	private String results_score2;
	private String results_team_id2;

	public int compareTo(MatchOfTheDayEntity outro) {

		return getBegin_at().compareTo(outro.getBegin_at());
	}

	public String getOpponent_type() {
		return opponent_type;
	}

	public void setOpponent_type(String opponent_type) {
		this.opponent_type = opponent_type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(String dateUpdate) {
		this.dateUpdate = dateUpdate;
	}

	public String getBegin_at() {
		return begin_at;
	}

	public void setBegin_at(String begin_at) {
		this.begin_at = begin_at;
	}

	public String getBegin_atConverted() {
		return begin_atConverted;
	}

	public void setBegin_atConverted(String begin_atConverted) {
		this.begin_atConverted = begin_atConverted;
	}

	public String getBeginHour() {
		return beginHour;
	}

	public void setBeginHour(String beginHour) {
		this.beginHour = beginHour;
	}

	public String getMatch_type() {
		return match_type;
	}

	public void setMatch_type(String match_type) {
		this.match_type = match_type;
	}

	public String getNumber_of_games() {
		return number_of_games;
	}

	public void setNumber_of_games(String number_of_games) {
		this.number_of_games = number_of_games;
	}

	public String getSerie_prize() {
		return serie_prize;
	}

	public void setSerie_prize(String serie_prize) {
		this.serie_prize = serie_prize;
	}

	public String getSerie_year() {
		return serie_year;
	}

	public void setSerie_year(String serie_year) {
		this.serie_year = serie_year;
	}

	public String getSerie_begin() {
		return serie_begin;
	}

	public void setSerie_begin(String serie_begin) {
		this.serie_begin = serie_begin;
	}

	public String getSerie_season() {
		return serie_season;
	}

	public void setSerie_season(String serie_season) {
		this.serie_season = serie_season;
	}

	public String getSerie_description() {
		return serie_description;
	}

	public void setSerie_description(String serie_description) {
		this.serie_description = serie_description;
	}

	public String getSerie_end_at() {
		return serie_end_at;
	}

	public void setSerie_end_at(String serie_end_at) {
		this.serie_end_at = serie_end_at;
	}

	public String getSerie_full_name() {
		return serie_full_name;
	}

	public void setSerie_full_name(String serie_full_name) {
		this.serie_full_name = serie_full_name;
	}

	public String getSerie_name() {
		return serie_name;
	}

	public void setSerie_name(String serie_name) {
		this.serie_name = serie_name;
	}

	public String getTournament_begin_at() {
		return tournament_begin_at;
	}

	public void setTournament_begin_at(String tournament_begin_at) {
		this.tournament_begin_at = tournament_begin_at;
	}

	public String getTournament_end_at() {
		return tournament_end_at;
	}

	public void setTournament_end_at(String tournament_end_at) {
		this.tournament_end_at = tournament_end_at;
	}

	public String getTournament_id() {
		return tournament_id;
	}

	public void setTournament_id(String tournament_id) {
		this.tournament_id = tournament_id;
	}

	public String getTournament_name() {
		return tournament_name;
	}

	public void setTournament_name(String tournament_name) {
		this.tournament_name = tournament_name;
	}

	public String getWinner() {
		return winner;
	}

	public void setWinner(String winner) {
		this.winner = winner;
	}

	public String getWinner_id() {
		return winner_id;
	}

	public void setWinner_id(String winner_id) {
		this.winner_id = winner_id;
	}

	public String getVideogame() {
		return videogame;
	}

	public void setVideogame(String videogame) {
		this.videogame = videogame;
	}

	public String getVideogame_img() {
		return videogame_img;
	}

	public void setVideogame_img(String videogame_img) {
		this.videogame_img = videogame_img;
	}

	public String getOpponent1_id() {
		return opponent1_id;
	}

	public void setOpponent1_id(String opponent1_id) {
		this.opponent1_id = opponent1_id;
	}

	public String getOpponent1_name() {
		return opponent1_name;
	}

	public void setOpponent1_name(String opponent1_name) {
		this.opponent1_name = opponent1_name;
	}

	public String getOpponent1_image_url() {
		return opponent1_image_url;
	}

	public void setOpponent1_image_url(String opponent1_image_url) {
		this.opponent1_image_url = opponent1_image_url;
	}

	public String getOpponent2_id() {
		return opponent2_id;
	}

	public void setOpponent2_id(String opponent2_id) {
		this.opponent2_id = opponent2_id;
	}

	public String getOpponent2_name() {
		return opponent2_name;
	}

	public void setOpponent2_name(String opponent2_name) {
		this.opponent2_name = opponent2_name;
	}

	public String getOpponent2_image_url() {
		return opponent2_image_url;
	}

	public void setOpponent2_image_url(String opponent2_image_url) {
		this.opponent2_image_url = opponent2_image_url;
	}

	public String getResults_score1() {
		return results_score1;
	}

	public void setResults_score1(String results_score1) {
		this.results_score1 = results_score1;
	}

	public String getResults_team_id1() {
		return results_team_id1;
	}

	public void setResults_team_id1(String results_team_id1) {
		this.results_team_id1 = results_team_id1;
	}

	public String getResults_score2() {
		return results_score2;
	}

	public void setResults_score2(String results_score2) {
		this.results_score2 = results_score2;
	}

	public String getResults_team_id2() {
		return results_team_id2;
	}

	public void setResults_team_id2(String results_team_id2) {
		this.results_team_id2 = results_team_id2;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "MatchOfTheDayEntity [id=" + id + ", dateUpdate=" + dateUpdate + ", begin_at=" + begin_at
				+ ", begin_atConverted=" + begin_atConverted + ", beginHour=" + beginHour + ", match_type=" + match_type
				+ ", number_of_games=" + number_of_games + ", serie_id=" + serie_id + ", serie_prize=" + serie_prize
				+ ", serie_year=" + serie_year + ", serie_begin=" + serie_begin + ", serie_season=" + serie_season
				+ ", serie_description=" + serie_description + ", serie_end_at=" + serie_end_at + ", serie_full_name="
				+ serie_full_name + ", serie_name=" + serie_name + ", tournament_begin_at=" + tournament_begin_at
				+ ", tournament_end_at=" + tournament_end_at + ", tournament_id=" + tournament_id + ", tournament_name="
				+ tournament_name + ", winner=" + winner + ", winner_id=" + winner_id + ", videogame=" + videogame
				+ ", videogame_img=" + videogame_img + ", opponent_type=" + opponent_type + ", opponent1_id="
				+ opponent1_id + ", opponent1_name=" + opponent1_name + ", opponent1_image_url=" + opponent1_image_url
				+ ", opponent2_id=" + opponent2_id + ", opponent2_name=" + opponent2_name + ", opponent2_image_url="
				+ opponent2_image_url + ", results_score1=" + results_score1 + ", results_team_id1=" + results_team_id1
				+ ", results_score2=" + results_score2 + ", results_team_id2=" + results_team_id2 + "]";
	}

}
