package com.avoidgroup.tournaments.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.stereotype.Component;

import com.avoidgroup.util.EntidadeBase;

@Component("GameEntity")
@Entity
public class GameEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String name;

	private String description;

	private String platform;

	private String image_default;

	private String image1;

	private String image2;

	private String image3;

	private String image4;

	private String image5;

	private String image_default_name;

	private String image1_name;

	private String image2_name;

	private String image3_name;

	private String image4_name;

	private String image5_name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getImage_default() {
		return image_default;
	}

	public void setImage_default(String image_default) {
		this.image_default = image_default;
	}

	public String getImage1() {
		return image1;
	}

	public void setImage1(String image1) {
		this.image1 = image1;
	}

	public String getImage2() {
		return image2;
	}

	public void setImage2(String image2) {
		this.image2 = image2;
	}

	public String getImage3() {
		return image3;
	}

	public void setImage3(String image3) {
		this.image3 = image3;
	}

	public String getImage4() {
		return image4;
	}

	public void setImage4(String image4) {
		this.image4 = image4;
	}

	public String getImage5() {
		return image5;
	}

	public void setImage5(String image5) {
		this.image5 = image5;
	}

	public String getImage_default_name() {
		return image_default_name;
	}

	public void setImage_default_name(String image_default_name) {
		this.image_default_name = image_default_name;
	}

	public String getImage1_name() {
		return image1_name;
	}

	public void setImage1_name(String image1_name) {
		this.image1_name = image1_name;
	}

	public String getImage2_name() {
		return image2_name;
	}

	public void setImage2_name(String image2_name) {
		this.image2_name = image2_name;
	}

	public String getImage3_name() {
		return image3_name;
	}

	public void setImage3_name(String image3_name) {
		this.image3_name = image3_name;
	}

	public String getImage4_name() {
		return image4_name;
	}

	public void setImage4_name(String image4_name) {
		this.image4_name = image4_name;
	}

	public String getImage5_name() {
		return image5_name;
	}

	public void setImage5_name(String image5_name) {
		this.image5_name = image5_name;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "GameEntity [id=" + id + ", name=" + name + ", description=" + description + ", platform=" + platform
				+ ", image_default=" + image_default + ", image1=" + image1 + ", image2=" + image2 + ", image3="
				+ image3 + ", image4=" + image4 + ", image5=" + image5 + ", image_default_name=" + image_default_name
				+ ", image1_name=" + image1_name + ", image2_name=" + image2_name + ", image3_name=" + image3_name
				+ ", image4_name=" + image4_name + ", image5_name=" + image5_name + "]";
	}

}
