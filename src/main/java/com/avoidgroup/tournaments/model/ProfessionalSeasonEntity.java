package com.avoidgroup.tournaments.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

import com.avoidgroup.util.EntidadeBase;

@Component("ProfessionalSeasonEntity")
@Entity
public class ProfessionalSeasonEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Temporal(TemporalType.DATE)
	private Date begin_date;

	@Temporal(TemporalType.DATE)
	private Date end_date;

	private String name;

	private String description;

	private String image_default;

	private String image_default_name;

	private String image1;

	private String image2;

	private String image1_name;

	private String image2_name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getBegin_date() {
		return begin_date;
	}

	public void setBegin_date(Date begin_date) {
		this.begin_date = begin_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage_default() {
		return image_default;
	}

	public void setImage_default(String image_default) {
		this.image_default = image_default;
	}

	public String getImage_default_name() {
		return image_default_name;
	}

	public void setImage_default_name(String image_default_name) {
		this.image_default_name = image_default_name;
	}

	public String getImage1() {
		return image1;
	}

	public void setImage1(String image1) {
		this.image1 = image1;
	}

	public String getImage2() {
		return image2;
	}

	public void setImage2(String image2) {
		this.image2 = image2;
	}

	public String getImage1_name() {
		return image1_name;
	}

	public void setImage1_name(String image1_name) {
		this.image1_name = image1_name;
	}

	public String getImage2_name() {
		return image2_name;
	}

	public void setImage2_name(String image2_name) {
		this.image2_name = image2_name;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ProfessionalSeasonEntity [id=" + id + ", begin_date=" + begin_date + ", end_date=" + end_date
				+ ", name=" + name + ", description=" + description + ", image_default=" + image_default
				+ ", image_default_name=" + image_default_name + ", image1=" + image1 + ", image2=" + image2
				+ ", image1_name=" + image1_name + ", image2_name=" + image2_name + "]";
	}

	
	
}
