package com.avoidgroup.team.controller;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.avoidgroup.dao.GenericDao;
import com.avoidgroup.model.ProfileEntity;
import com.avoidgroup.team.model.TeamAdminEntity;
import com.avoidgroup.team.model.TeamEntity;
import com.avoidgroup.team.model.TeamFaqEntity;
import com.avoidgroup.team.model.TeamProfileEntity;
import com.avoidgroup.team.model.TeamPublicationEntity;
import com.avoidgroup.team.model.TeamRequestEntity;

@Controller
@RequestMapping(value = "/team")
public class TeamFaqController {
	@Autowired
	private GenericDao<TeamFaqEntity> daoFaq;
	@Autowired
	private GenericDao<TeamEntity> daoTeam;
	@Autowired
	private TeamEntity team;
	@Autowired
	private TeamFaqEntity faq;

	@Autowired
	private List<TeamFaqEntity> listFaq;

	@Autowired
	private ProfileEntity profile;
	@Autowired
	private GenericDao<ProfileEntity> daoProfile;

	@RequestMapping(value = "/viewFaq/{id}", method = RequestMethod.GET)
	public ModelAndView viewfaq(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {

		Map<String, Object> mapTeam = new HashMap<String, Object>();
		mapTeam.put("id", Integer.parseInt(id));
		if (daoTeam.exist(TeamEntity.class, mapTeam, "")) {
			team = daoTeam.buscaId(TeamEntity.class, Integer.parseInt(id));

			profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");
			if (team.getOwner().getId().equals(profile.getId())) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("team.id", Integer.parseInt(id));
				map.put("status", "open");

				listFaq = daoFaq.listarProperty(TeamFaqEntity.class, map, "and");

				model.addObject("lista", listFaq);
				model.addObject("team", team);
				model.setViewName("team/faq/open");
				return model;
			} else {

				model.setViewName("redirect:/team/viewTeam/" + team.getId().toString());
				return model;
			}
		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/faq/delete/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));
		if (daoFaq.exist(TeamFaqEntity.class, map, "and")) {
			faq = daoFaq.buscaId(TeamFaqEntity.class, Integer.parseInt(id));
			String teamID = faq.getTeam().getId().toString();

			profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");
			if (faq.getTeam().getOwner().getId().equals(profile.getId())) {
				daoFaq.remove(TeamFaqEntity.class, Integer.parseInt(id));

				model.setViewName("redirect:/team/viewTeam/" + teamID);
				return model;
			} else {

				model.setViewName("redirect:/team/viewTeam/" + teamID);
				return model;
			}
		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/faq/answer", method = RequestMethod.POST)
	public ModelAndView answer(TeamFaqEntity faqEntity, String faqID, ModelAndView model, HttpServletRequest request) {

		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		faq = daoFaq.buscaId(TeamFaqEntity.class, Integer.parseInt(faqID));

		String teamID = faq.getTeam().getId().toString();

		if (faq.getTeam().getOwner().getId().equals(profile.getId())) {

			Calendar date1 = Calendar.getInstance();
			faq.setAnswer(faqEntity.getAnswer());
			faq.setDateOfAnswer(date1.getTime());
			faq.setTimeOfAnswer(date1.getTime());
			faq.setStatus("done");
			faq.setResponse(profile);

			daoFaq.saveUpdate(faq);

			model.setViewName("redirect:/team/viewFaq/" + teamID);
			return model;
		} else {

			model.setViewName("redirect:/team/viewTeam/" + teamID);
			return model;
		}

	}

	@RequestMapping(value = "/faq/{id}", method = RequestMethod.GET)
	public ModelAndView faq(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));
		if (daoTeam.exist(TeamEntity.class, map, "and")) {
			team = daoTeam.buscaId(TeamEntity.class, Integer.parseInt(id));

			Map<String, Object> mapTeam = new HashMap<String, Object>();
			mapTeam.put("team.id", Integer.parseInt(id));
			mapTeam.put("status", "done");

			listFaq = daoFaq.listarProperty(TeamFaqEntity.class, mapTeam, "and");

			model.addObject("lista", listFaq);
			model.addObject("team", team);
			model.setViewName("team/faq/faq");
			return model;
		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/faq/save", method = RequestMethod.POST)
	public ModelAndView save(TeamFaqEntity faqEntity, String teamID, ModelAndView model, HttpServletRequest request) {

		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		team = daoTeam.buscaId(TeamEntity.class, Integer.parseInt(teamID));

		Calendar date1 = Calendar.getInstance();

		faqEntity.setTimeOfQuestion(date1.getTime());
		faqEntity.setDateOfQuestion(date1.getTime());
		faqEntity.setProfile(profile);
		faqEntity.setStatus("open");
		faqEntity.setTeam(team);

		daoFaq.saveUpdate(faqEntity);

		model.setViewName("redirect:/team/faq/" + teamID);
		return model;

	}
}
