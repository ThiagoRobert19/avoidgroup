package com.avoidgroup.team.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.avoidgroup.dao.GenericDao;
import com.avoidgroup.model.InvitationEntity;
import com.avoidgroup.model.ProfileEntity;
import com.avoidgroup.model.UserEntity;
import com.avoidgroup.team.model.TeamEntity;
import com.avoidgroup.team.model.TeamProfileEntity;
import com.avoidgroup.team.model.TeamPublicationEntity;
import com.avoidgroup.util.Common;
import com.avoidgroup.util.DropBoxUtil;

@Controller
@RequestMapping(value = "/teamPublication")
public class TeamPublicationController {

	@Autowired
	private Common common;

	@Autowired
	private GenericDao<ProfileEntity> daoProfile;

	@Autowired
	private GenericDao<TeamEntity> daoTeam;

	@Autowired
	private GenericDao<TeamProfileEntity> daoTeamProfile;

	@Autowired
	private GenericDao<TeamPublicationEntity> daoTeamPublication;

	@Autowired
	private UserEntity userEntity;

	@Autowired
	private TeamPublicationEntity teamPublication;

	@Autowired
	private ProfileEntity profile;

	@Autowired
	private TeamEntity team;

	@RequestMapping(value = "/publication/delete/{id}", method = RequestMethod.GET)
	public ModelAndView accept(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));
		if (daoTeamPublication.exist(TeamPublicationEntity.class, map, "and")) {
			teamPublication = daoTeamPublication.buscaId(TeamPublicationEntity.class, Integer.parseInt(id));
			Integer teamId = teamPublication.getTeam().getId();

			if (teamPublication.getImage_name() != null && !teamPublication.getImage_name().equals("")) {
				DropBoxUtil.deleteFile(teamPublication.getImage_name());
			}

			daoTeamPublication.remove(TeamPublicationEntity.class, teamPublication.getId());

			model.setViewName("redirect:/team/viewTeam/" + teamId);

			return model;
		} else {
			model.setViewName("redirect:/");

			return model;
		}

	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView save(String content, MultipartFile file, String idTeam, HttpServletRequest request,
			ModelAndView model) throws IllegalStateException, IOException {

		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");
		team = daoTeam.buscaId(TeamEntity.class, Integer.parseInt(idTeam));

		Map<String, Object> mapTeamProfile = new HashMap<String, Object>();
		mapTeamProfile.put("team.id", team.getId());
		mapTeamProfile.put("profile.id", profile.getId());

		if (daoTeamProfile.exist(TeamProfileEntity.class, mapTeamProfile, "and")) {

			Calendar g = new GregorianCalendar();
			teamPublication.setContent(content);
			teamPublication.setPublisher(profile);
			teamPublication.setTeam(team);
			teamPublication.setTimeOfComment(g.getTime());
			teamPublication.setDateOfComment(g.getTime());

			if (file.toString() != null && !file.getOriginalFilename().equals("")) {
				System.out.println("nao eh null");
				File convFile = new File(request.getRealPath("/img/") + file.getOriginalFilename());

				file.transferTo(convFile);

				SimpleDateFormat df = new SimpleDateFormat("ddMMyyyyHHmmss");
				final Calendar cal = Calendar.getInstance();

				String nome = profile.getFirstName().toLowerCase() + profile.getLastName().toLowerCase()
						+ userEntity.getId() + df.format(cal.getTime()) + "publication";

				String foto = DropBoxUtil.uploadFile(convFile, "/" + nome.trim() + ".jpg");
				teamPublication.setImage_name("/" + nome.trim() + ".jpg");
				teamPublication.setImage(foto);

			} else {
				teamPublication.setImage(null);
			}

			daoTeamPublication.saveUpdate(teamPublication);

			model.setViewName("redirect:/team/viewTeam/" + team.getId() + "");

			return model;
		}

		else {
			model.setViewName("team/viewTeam/" + team.getId() + "");
			model.addObject("erro", "You must be part of the Team to use this feature");

			return model;
		}

	}

}
