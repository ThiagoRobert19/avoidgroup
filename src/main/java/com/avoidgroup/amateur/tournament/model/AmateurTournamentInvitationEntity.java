package com.avoidgroup.amateur.tournament.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

import com.avoidgroup.team.model.TeamEntity;
import com.avoidgroup.util.EntidadeBase;

@Component("AmateurTournamentInvitationEntity")
@Entity
public class AmateurTournamentInvitationEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String status;

	@Temporal(TemporalType.DATE)
	private Date dateOfInvitation;

	@OneToOne
	@JoinColumn(name = "invited_id")
	private TeamEntity invited;

	@OneToOne
	@JoinColumn(name = "tournament_id")
	private AmateurTournamentEntity tournament;

	public AmateurTournamentEntity getTournament() {
		return tournament;
	}

	public void setTournament(AmateurTournamentEntity tournament) {
		this.tournament = tournament;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDateOfInvitation() {
		return dateOfInvitation;
	}

	public void setDateOfInvitation(Date dateOfInvitation) {
		this.dateOfInvitation = dateOfInvitation;
	}

	public TeamEntity getInvited() {
		return invited;
	}

	public void setInvited(TeamEntity invited) {
		this.invited = invited;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "AmateurTournamentInvitationEntity [id=" + id + ", status=" + status + ", dateOfInvitation="
				+ dateOfInvitation + ", invited=" + invited + ", tournament=" + tournament + "]";
	}

}
