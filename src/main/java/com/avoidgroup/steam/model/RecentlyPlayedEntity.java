package com.avoidgroup.steam.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.avoidgroup.model.ProfileEntity;
import com.avoidgroup.util.EntidadeBase;

@Entity
public class RecentlyPlayedEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private int appid;

	@ManyToOne
	private ProfileEntity profile;
	
	private String name;
	private String playtime_2weeks;
	private String playtime_forever;
	private String imageURL;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getAppid() {
		return appid;
	}
	public void setAppid(int appid) {
		this.appid = appid;
	}
	public ProfileEntity getProfile() {
		return profile;
	}
	public void setProfile(ProfileEntity profile) {
		this.profile = profile;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPlaytime_2weeks() {
		return playtime_2weeks;
	}
	public void setPlaytime_2weeks(String playtime_2weeks) {
		this.playtime_2weeks = playtime_2weeks;
	}
	public String getPlaytime_forever() {
		return playtime_forever;
	}
	public void setPlaytime_forever(String playtime_forever) {
		this.playtime_forever = playtime_forever;
	}
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "RecentlyPlayedEntity [id=" + id + ", appid=" + appid + ", profile=" + profile + ", name=" + name
				+ ", playtime_2weeks=" + playtime_2weeks + ", playtime_forever=" + playtime_forever + ", imageURL="
				+ imageURL + "]";
	}
	


}
