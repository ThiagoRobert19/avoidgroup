<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:import url="/WEB-INF/cabecalho/header.jsp" />
<section class="interface-signup">

	<!-- Terms -->
	<section id="terms" class="terms-class">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="modal-content">
						<div class="modal-header">
							<p>Terms of Service</p>
							<span class="close">&times;</span>
						</div>
						<div class="modal-body">
							<iframe src="<c:url value='/avoidgroup/terms'/>" width="560"
								height="315" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="modal-footer">
							<span class="icon-avoid-login"><img
								src="<c:url value='/resources/_css/iconfonts/logo.svg'/>"
								alt="[Avoid Group]"></span> <span class="sr-only">Avoid
								Group</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Terms -->

	<div class="row">
		<!-- Testimonials -->
		<div class="col-md-6 section-testimonials-block">
			<section class="section-testimonials text-center bg-dark">
				<div class="container">
					<h3 class="sr-only">Comentarios</h3>
					<div id="carousel-testimonials" class="carousel slide"
						data-ride="carousel" data-interval="0">
						<div class="carousel-inner">
							<div class="carousel-item">
								<blockquote class="blockquote">
									<img src="<c:url value='/resources/_img/_users/face1.jpg'/>"
										height="80" width="80" alt="[Cold]" class="rounded-circle">
									<p class="h3">Lorem ipsum dolor sit amet, consectetur
										adipisicing elit. Magni pariatur cupiditate, architecto iste
										exercitationem ea, animi eius mollitia veritatis sequi, vero
										voluptas, explicabo dolorem minima ducimus quibusdam dolores
										ut rerum?</p>
									<footer>Cold</footer>
								</blockquote>
							</div>
							<div class="carousel-item">
								<blockquote class="blockquote">
									<img src="<c:url value='/resources/_img/_users/face2.jpg'/>"
										height="80" width="80" alt="[Taco]" class="rounded-circle">
									<p class="h3">Lorem ipsum dolor sit amet, consectetur
										adipisicing elit. Magni pariatur cupiditate, architecto iste
										exercitationem ea, animi eius mollitia veritatis sequi, vero
										voluptas, explicabo dolorem minima ducimus quibusdam dolores
										ut rerum?.</p>
									<footer>Taco</footer>
								</blockquote>
							</div>
							<div class="carousel-item active">
								<blockquote class="blockquote">
									<img src="<c:url value='/resources/_img/_users/face3.jpg'/>"
										height="80" width="80" alt="[Fer]" class="rounded-circle">
									<p class="h3">Lorem ipsum dolor sit amet, consectetur
										adipisicing elit. Magni pariatur cupiditate, architecto iste
										exercitationem ea, animi eius mollitia veritatis sequi, vero
										voluptas, explicabo dolorem minima ducimus quibusdam dolores
										ut rerum?</p>
									<footer>Fer</footer>
								</blockquote>
							</div>
							<div class="carousel-item">
								<blockquote class="blockquote">
									<img src="<c:url value='/resources/_img/_users/face4.jpg'/>"
										height="80" width="80" alt="[Felps]" class="rounded-circle">
									<p class="h3">Lorem ipsum dolor sit amet, consectetur
										adipisicing elit. Magni pariatur cupiditate, architecto iste
										exercitationem ea, animi eius mollitia veritatis sequi, vero
										voluptas, explicabo dolorem minima ducimus quibusdam dolores
										ut rerum?</p>
									<footer>Felps</footer>
								</blockquote>
							</div>
							<div class="carousel-item">
								<blockquote class="blockquote">
									<img src="<c:url value='/resources/_img/_users/face5.jpg'/>"
										height="80" width="80" alt="[Fallen]" class="rounded-circle">
									<p class="h3">Lorem ipsum dolor sit amet, consectetur
										adipisicing elit. Magni pariatur cupiditate, architecto iste
										exercitationem ea, animi eius mollitia veritatis sequi, vero
										voluptas, explicabo dolorem minima ducimus quibusdam dolores
										ut rerum?.</p>
									<footer>Fallen</footer>
								</blockquote>
							</div>
						</div>
						<ol class="carousel-indicators">
							<li data-target="#carousel-testimonials" data-slide-to="0"><img
								src="<c:url value='/resources/_img/_users/face1.jpg'/>"
								alt="Navigation avatar" class="img-fluid rounded-circle"></li>
							<li data-target="#carousel-testimonials" data-slide-to="1"><img
								src="<c:url value='/resources/_img/_users/face2.jpg'/>"
								alt="Navigation avatar" class="img-fluid rounded-circle"></li>
							<li data-target="#carousel-testimonials" data-slide-to="2"
								class="active"><img
								src="<c:url value='/resources/_img/_users/face3.jpg'/>"
								alt="Navigation avatar" class="img-fluid rounded-circle"></li>
							<li data-target="#carousel-testimonials" data-slide-to="3"><img
								src="<c:url value='/resources/_img/_users/face4.jpg'/>"
								alt="Navigation avatar" class="img-fluid rounded-circle"></li>
							<li data-target="#carousel-testimonials" data-slide-to="4"><img
								src="<c:url value='/resources/_img/_users/face5.jpg'/>"
								alt="Navigation avatar" class="img-fluid rounded-circle"></li>
						</ol>
					</div>
				</div>
			</section>
		</div>
		<!-- Testimonials -->

		<!-- Form-->
		<div class="col-md-6 form-login-block form-login px-5">
			<a class="navbar-brand mr-auto" href="<c:url value='/'/>"> <span
				class="icon-avoid-login"><img
					src="<c:url value='/resources/_css/iconfonts/logo.svg'/>"
					alt="[Avoid Group]"></span> <span class="sr-only">Avoid Group</span>
			</a>
			<!-- Form Acess -->
			<div class="cd-user-modal">
				<ul class="cd-switcher">
					<li class="btn btn-lg"><a href="#0">Sign in</a></li>
					<li class="btn btn-lg"><a href="#0">New account</a></li>
				</ul>



				<!-- cd login -->
				<div id="cd-login" class="is-selected">
					<form class="cd-form" action="<c:url value='/user/login/simple'/>"
						method="post">

						<div class="form-group has-icon-left form-control-email">
							<label class="sr-only" for="inputEmail">Your email</label> <input
								type="email" class="form-control form-control-lg"
								id="inputEmail" name="email"
								placeholder="Enter your email address" autocomplete="on">
							<span class="cd-error-message">erro</span>
						</div>

						<div class="form-group has-icon-left form-control-password">
							<label class="sr-only" for="inputPassword">Enter a
								password</label> <input type="password"
								class="form-control form-control-lg" id="inputPassword"
								name="password" placeholder="Password" autocomplete="on">
							<a href="#0" class="hide-password">Show</a> <span
								class="cd-error-message">Error message here!</span>
						</div>

						<button type="submit" class="btn btn-primary btn-block btn-lg">Log
							In</button>
					</form>
					<c:if test="${not empty erro}">
						<p class="cd-form-bottom-message btn btn-lg">
							<a href="#0">${erro}</a>
						</p>

					</c:if>
					<p class="cd-form-bottom-message btn btn-lg">
						<a href="#0">Forgot your password?</a>
					</p>
				</div>
				<!-- cd-login -->

				<div id="cd-signup">
					<!-- sign up form -->
					<form class="cd-form" action="<c:url value='/user/verify'/>"
						method="post">

						<div class="form-group has-icon-left form-control-email">
							<label class="sr-only" for="inputLogEmail">Your email</label> <input
								type="email" class="form-control form-control-lg"
								id="inputLogEmail" placeholder="Informe o seu Email"
								name="email"> <span class="cd-error-message">Error
								message here!</span>
						</div>

						<div class="form-group has-icon-left form-control-password">
							<label class="sr-only" for="inputLogPassword">Enter Your
								password</label> <input type="password"
								class="form-control form-control-lg" id="inputLogPassword"
								name="password" placeholder="Informe uma Senha"
								autocomplete="on"> <a href="#0" class="hide-password">Show</a>
							<span class="cd-error-message">Error message here!</span>
						</div>

						<div class="form-group has-icon-left form-control-password">
							<label class="sr-only" for="inputLogRePassword">Re-type
								Your password</label> <input type="password"
								class="form-control form-control-lg" id="inputLogRePassword"
								name="confirmPassword" placeholder="Confirme a Senha"
								autocomplete="on"> <a href="#0" class="hide-password">Show</a>
							<span class="cd-error-message">Error message here!</span>
						</div>

						<label class="c-input c-checkbox"> <input type="checkbox"
							checked> <span class="c-indicator"></span> I agree to
							Avoid <a href="#" id="myBtn" class="c-input-terms">terms of
								service</a>
						</label>

						<button type="submit"
							onclick="window.location.href='<c:url value='/user/verify'/>'"
							class="btn btn-primary btn-block btn-lg">Sign up</button>

					</form>
				</div>
				<!-- cd-signup -->

				<div id="cd-reset-password">
					<!-- reset password form -->
					<p class="cd-form-message">Lost your password? Please enter
						your email address. You will receive a link to create a new
						password.</p>


					<form class="cd-form" action="<c:url value='/user/resetPassword'/>"
						method="post">
						<div class="form-group has-icon-left form-control-email">
							<label class="sr-only" for="reset-email">Your
								email-Recovered</label> <input type="email"
								class="form-control form-control-lg" id="reset-email"
								placeholder="Email address" name="resetByEmail"
								autocomplete="on" required="required">

						</div>

						<button type="submit" class="btn btn-primary btn-danger btn-lg">Reset
							Password</button>


						<hr class="invisible">
					</form>

					<p class="cd-form-bottom-message btn btn-lg">
						<a href="#0">Back to log-in</a>
					</p>
				</div>
				<!-- cd-reset-password -->
			</div>
			<!-- Form Acess -->

			<!-- Btn Acess -->
			<section class="form-login-btn">
				<div class="form-login-social text-center">
					<h3 class="pb-2">Or</h3>
					<a href="#" class="btn btn-login-steam" title="Steam"> <span
						class="fab fa-steam"></span>
					</a> <a href="#" class="btn btn-login-xbox" title="Xbox"> <span
						class="fab fa-xbox"></span>
					</a> <a href="#" class="btn btn-login-twitch" title="Twitch"> <span
						class="fab fa-twitch"></span>
					</a> <a href="#"
						class="btn btn-social btn-social-icon btn-social-facebook"
						title="Facebook"> <span class="icon-facebook"></span>
					</a>
				</div>
			</section>
			<!-- Btn Acess -->
		</div>
		<!-- form login block -->

	</div>
	<!-- row -->
</section>
<!-- Interface -->

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="<c:url value='/resources/_js/script-lib-cst.js'/>"></script>
<script src="<c:url value='/resources/_js/script.js'/>"></script>
<script>
	var modal = document.getElementById('terms');
	var btn = document.getElementById("myBtn");
	var span = document.getElementsByClassName("close")[0];

	btn.onclick = function() {
		modal.style.display = "block";
	}

	span.onclick = function() {
		modal.style.display = "none";
	}

	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
	}
</script>


</main>
</body>

</html>
