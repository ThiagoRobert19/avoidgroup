package com.avoidgroup.esports.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import java.util.List;

import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.avoidgroup.dao.GenericDao;
import com.avoidgroup.esports.model.MatchOfTheDayEntity;
import com.avoidgroup.esports.util.MatchesUtil;

import com.avoidgroup.model.ProfileEntity;

@Controller
@RequestMapping(value = "/esport")
public class SportsMatchesController {

	@Autowired
	private List<MatchOfTheDayEntity> listSport;
	@Autowired
	private List<MatchOfTheDayEntity> listSport2;

	@Autowired
	private GenericDao<MatchOfTheDayEntity> daoMatchOfTheDay;

	@Autowired
	private ProfileEntity profile;

	@RequestMapping(value = "/matches/today", method = RequestMethod.GET)
	public ModelAndView matchesofthedayNEW(HttpServletRequest request, ModelAndView model)
			throws JSONException, ParseException {

		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		TimeZone zone = TimeZone.getTimeZone(profile.getTimeZone());

		SimpleDateFormat sd = new SimpleDateFormat("yyyy/MM/dd");
		sd.setTimeZone(zone);
		Date d = Calendar.getInstance().getTime();
		String today = sd.format(d);

		listSport = daoMatchOfTheDay.list(MatchOfTheDayEntity.class);
		listSport2.clear();
		listSport2 = getToday(listSport, today);
		Collections.sort(listSport2);
		model.addObject("listSport", listSport2);
		model.setViewName("esport/today");
		return model;

	}

	@SuppressWarnings("finally")
	public List<MatchOfTheDayEntity> getToday(List<MatchOfTheDayEntity> listaPass, String today) {
		List<MatchOfTheDayEntity> list = new ArrayList<MatchOfTheDayEntity>();

		MatchesUtil util = new MatchesUtil();
		for (MatchOfTheDayEntity mpassa : listSport) {
			if (mpassa.getBegin_at() != null && !mpassa.getBegin_at().toString().equals("")
					&& mpassa.getBegin_at().toString() != null) {

			}
			try {
				String timeZ = profile.getTimeZone().trim();
				String datet = mpassa.getBegin_at();

				datet = util.Converta(datet, profile);
				String hr = util.getHoraByTimeZone(timeZ, datet);
				String day = util.getDataByTimeZone(timeZ, datet);
				if (day.equals(today)) {
					mpassa.setBeginHour(hr);
					mpassa.setBegin_atConverted(day);

					list.add(mpassa);
				}

			} finally {
				continue;
			}
		}
		return list;

	}

}
