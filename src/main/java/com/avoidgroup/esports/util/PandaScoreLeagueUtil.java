package com.avoidgroup.esports.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONArray;
import org.json.JSONException;

public class PandaScoreLeagueUtil {
	private static final String ACCESS_TOKEN = "sL3PReaZ8uJPnbKFbMrIEukZ7rOxpaEQ-xmPRwNkHl_sBfB9cLc";
	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	private static JSONArray readJsonFromUrl(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JSONArray values = new JSONArray(jsonText);
			return values;
		} finally {
			is.close();
		}
	}
	public JSONArray GetLeagues() {

		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/leagues?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			System.out.println("erro: " + e.getMessage().toString());
			System.out.println("erro: " + e.toString());
			return null;
		}

	}
	public JSONArray GetALeague(String leagueID) {

		try {
			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/leagues/"+leagueID+"?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
	
			return null;
		}

	}
	public JSONArray GetMachesForALeague(String leagueID) {
		try {
			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/leagues/"+leagueID+"/matches?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
		
			return null;
		}

	}
	public JSONArray GetPastMachesForALeague(String leagueID) {
		try {
			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/leagues/"+leagueID+"/matches/past?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			
			return null;
		}

	}
	public JSONArray GetRunningMachesForALeague(String leagueID) {
		try {
			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/leagues/"+leagueID+"/matches/running?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
	public JSONArray GetUpcomingMachesForALeague(String leagueID) {
		try {
			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/leagues/"+leagueID+"/matches/upcoming?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
	public JSONArray GetListSeriesOfALeague(String leagueID) {
		try {
			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/leagues/"+leagueID+"/series?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
	public JSONArray GetListTournamentsOfALeague(String leagueID) {
		try {
			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/leagues/"+leagueID+"/tournaments?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
}
