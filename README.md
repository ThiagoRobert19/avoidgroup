**Avoid Group LLC**
A product of AVOID GROUP LLC

The purpose of this project is to create a social media focused on games plataforms such as Steam,
 PlayStation, and XBOX where people can communicate, exchange items, create teams, communities.

**Developers**
- Lucas Jeronimo
- David Muniz
- Thiago Robert Prado Souza

**Database**
Postegres

**Server**
Tomcat 8

*********************** TO DO LIST ***********************

- [x] Validacao dos controllers
- [x] Integracao com Twitch TV API
- [x] PAndaScore API para ESports
- [x] Uso do Interceptor
- [ ] Criar Torneios Amadores
- [ ] Criar Torneios Pro
- [x] Criar projeto para gerenciar parte administrativa
- [x] Criacao de noticias na parte Administrativa
- [x] DropBox API
- [ ] Criacao de Portal para conteudo, Discussao, comparacoes de games, perifericos e novidades tecnologicas na plataforma Avoid
- [ ] Decisao de como sera a monetizacao da plataforma.
- [ ] Inclusao do STRIPE API para pagamento


*********************** TO DO LIST (Front) ***********************

- [ ] Adjust for java sign up page ( log(new).jsp )
- [x] Terms of license (Modal Window)
- [ ] Footer
- [x] Update width for 1366px x 768px
- [x] Adjust sign-up page
- [ ] Adjust publishing session
- [ ] Include login and sign-up (Modal Window) on menu
- [ ] Ranking page
- [ ] Include brackets tournaments on ranking page
- [ ] Adjust email page
- [ ] Adjust search option on menu
- [ ] Adjust profile Page 
- [ ] Include comments on News Feed page
- [ ] Include tooltip on News Feed Page
- [ ] SEO html
