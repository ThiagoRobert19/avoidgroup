package com.avoidgroup.esports.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONArray;
import org.json.JSONException;

public class PandaScoreTeamsUtil {

	private static final String ACCESS_TOKEN = "sL3PReaZ8uJPnbKFbMrIEukZ7rOxpaEQ-xmPRwNkHl_sBfB9cLc";
	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	private static JSONArray readJsonFromUrl(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			// JSONObject json = new JSONObject(jsonText);
			JSONArray values = new JSONArray(jsonText);
			return values;
		} finally {
			is.close();
		}
	}
	public JSONArray GetTeams() {

		try {
			// https://api.pandascore.co/lives?page=1&per_page=100?&token=sL3PReaZ8uJPnbKFbMrIEukZ7rOxpaEQ-xmPRwNkHl_sBfB9cLc
			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/teams?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
	public JSONArray GetATeam(String teamID) {

		try {
			// https://api.pandascore.co/lives?page=1&per_page=100?&token=sL3PReaZ8uJPnbKFbMrIEukZ7rOxpaEQ-xmPRwNkHl_sBfB9cLc
			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/teams/"+teamID+"?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
	public JSONArray GetLeaguesForATeam(String teamID) {

		try {
			// https://api.pandascore.co/lives?page=1&per_page=100?&token=sL3PReaZ8uJPnbKFbMrIEukZ7rOxpaEQ-xmPRwNkHl_sBfB9cLc
			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/teams/"+teamID+"/leagues?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
	public JSONArray GetMatchesForATeam(String teamID) {

		try {
			// https://api.pandascore.co/lives?page=1&per_page=100?&token=sL3PReaZ8uJPnbKFbMrIEukZ7rOxpaEQ-xmPRwNkHl_sBfB9cLc
			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/teams/"+teamID+"/matches?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
	public JSONArray GetSeriesForATeam(String teamID) {

		try {
			// https://api.pandascore.co/lives?page=1&per_page=100?&token=sL3PReaZ8uJPnbKFbMrIEukZ7rOxpaEQ-xmPRwNkHl_sBfB9cLc
			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/teams/"+teamID+"/series?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
	public JSONArray GetTournamentsForATeam(String teamID) {

		try {
			// https://api.pandascore.co/lives?page=1&per_page=100?&token=sL3PReaZ8uJPnbKFbMrIEukZ7rOxpaEQ-xmPRwNkHl_sBfB9cLc
			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/teams/"+teamID+"/tournaments?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
}
