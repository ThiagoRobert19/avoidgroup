package com.avoidgroup.steam.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.avoidgroup.util.EntidadeBase;

@Entity
public class SummarieEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String personaname;
	private String avatarmedium;
	private String lastlogoff;
	private String profileurl;
	private String timecreated;

	
	private String loccountrycode;
	
	private String loccityid;
	
	private String locstatecode;
	private String level;
	
	
	
	
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getLocstatecode() {
		return locstatecode;
	}
	public void setLocstatecode(String locstatecode) {
		this.locstatecode = locstatecode;
	}
	public String getLoccityid() {
		return loccityid;
	}
	public void setLoccityid(String loccityid) {
		this.loccityid = loccityid;
	}
	public String getLoccountrycode() {
		return loccountrycode;
	}
	public void setLoccountrycode(String loccountrycode) {
		this.loccountrycode = loccountrycode;
	}
	
	public String getTimecreated() {
		return timecreated;
	}
	public void setTimecreated(String timecreated) {
		this.timecreated = timecreated;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPersonaname() {
		return personaname;
	}
	public void setPersonaname(String personaname) {
		this.personaname = personaname;
	}
	public String getAvatarmedium() {
		return avatarmedium;
	}
	public void setAvatarmedium(String avatarmedium) {
		this.avatarmedium = avatarmedium;
	}
	public String getLastlogoff() {
		return lastlogoff;
	}
	public void setLastlogoff(String lastlogoff) {
		this.lastlogoff = lastlogoff;
	}
	public String getProfileurl() {
		return profileurl;
	}
	public void setProfileurl(String profileurl) {
		this.profileurl = profileurl;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "SummarieEntity [id=" + id + ", personaname=" + personaname + ", avatarmedium=" + avatarmedium
				+ ", lastlogoff=" + lastlogoff + ", profileurl=" + profileurl + ", timecreated=" + timecreated + "]";
	}
	



}
