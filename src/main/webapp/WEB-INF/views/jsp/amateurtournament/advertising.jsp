<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div class="col-md-6 col-xl-2">
	<hr class="invisible">
	<div class="card gedf-card wp wp-5">
		<div class="card-body">
			<h5 class="card-title">Banner com fotos</h5>
			<h6 class="card-subtitle mb-2 text-muted">Card Ads</h6>
			<p class="card-text">Some quick example text to build on the card
				title and make up the bulk of the card's content.</p>
			<a href="#" class="card-link">Card link</a> <a href="#"
				class="card-link">Another link</a>
		</div>
	</div>
	<section class="wp wp-6">
		<div class="card">
			<ul class="list-group list-group-flush">
				<li class="list-group-item">
					<div class="h6 text-muted">
						<a href="#">Quests</a>
					</div>
				</li>
				<li class="list-group-item">
					<div class="h6 text-muted">
						<a href="#">Events</a>
					</div>
				</li>

				<li class="list-group-item">
					<div class="h6 text-muted">
						<a href="#">Training</a>
					</div>
				</li>
				<li class="list-group-item">
					<div class="h6 text-muted">
						<a href="#">View Tournaments</a>
					</div>
				</li>
				<li class="list-group-item">
					<div class="h6 text-muted">
						<a href="#">Opinion</a>
					</div>
				</li>
				
			</ul>
		</div>
	</section>
	<hr class="invisible">
</div>