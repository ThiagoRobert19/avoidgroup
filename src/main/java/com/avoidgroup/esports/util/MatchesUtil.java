package com.avoidgroup.esports.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.avoidgroup.model.ProfileEntity;

public class MatchesUtil {

	public String getHoraByTimeZone(String timeZone, String dataOLD) throws ParseException {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

		TimeZone zone = TimeZone.getTimeZone(timeZone);
		if (!timeZone.equals("America_New_York")) {
			dateFormat.setTimeZone(zone);
		}
		

		Date datet = dateFormat.parse(dataOLD);

		DateFormat hora = new SimpleDateFormat("HH:mm");
		// hora.setTimeZone(zone);
		String horaStr = hora.format(datet);
		return horaStr;
	}

	public String getDataByTimeZone(String timeZone, String dataOLD) throws ParseException {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

		TimeZone zone = TimeZone.getTimeZone(timeZone);
		if (!timeZone.equals("America_New_York")) {
			dateFormat.setTimeZone(zone);
		}
		

		Date datet = dateFormat.parse(dataOLD);

		DateFormat day = new SimpleDateFormat("yyyy/MM/dd");
		// hora.setTimeZone(zone);
		String daySTR = day.format(datet);
		return daySTR;
	}

	public String Converta(String d,ProfileEntity profile) throws ParseException {
		
		Integer diff=0;
		if(profile.getCountry().equals("Brazil")){
			diff=-2;
		}
		if(profile.getCountry().equals("United States")){
			diff=-3;
		}
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		
		
		Date datet = dateFormat.parse(d);
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(datet);

		// Substract 2 hour from the current time
		calendar.add(Calendar.HOUR, diff);

		Date datet2 = calendar.getTime();
		return dateFormat.format(datet2);
	}
}
