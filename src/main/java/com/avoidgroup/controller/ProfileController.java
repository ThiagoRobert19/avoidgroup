package com.avoidgroup.controller;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.avoidgroup.dao.GenericDao;
import com.avoidgroup.model.AboutFeaturedEntity;
import com.avoidgroup.model.FeaturedGamesEntity;
import com.avoidgroup.model.FriendEntity;
import com.avoidgroup.model.GeneralCommentEntity;
import com.avoidgroup.model.GeneralLikeEntity;
import com.avoidgroup.model.GeneralPublicationEntity;
import com.avoidgroup.model.InvitationEntity;
import com.avoidgroup.model.ProfileEntity;
import com.avoidgroup.model.UserEntity;
import com.avoidgroup.steam.model.ProfileSteamEntity;
import com.avoidgroup.team.model.TeamAdminEntity;
import com.avoidgroup.team.model.TeamEntity;
import com.avoidgroup.team.model.TeamProfileEntity;
import com.avoidgroup.team.model.TeamPublicationEntity;
import com.avoidgroup.team.model.TeamRequestEntity;
import com.avoidgroup.twitchapi.model.ProfileTwitchEntity;
import com.avoidgroup.util.Common;
import com.avoidgroup.util.Criptografia;
import com.avoidgroup.util.CulturaScope;
import com.avoidgroup.util.DropBoxUtil;
import com.google.gson.Gson;

@Controller
@RequestMapping(value = "/profile")
public class ProfileController {
	@Autowired
	private CulturaScope cultura;
	@Autowired
	private GenericDao<GeneralLikeEntity> daoLike;

	@Autowired
	private GenericDao<GeneralPublicationEntity> daoPublication;

	@Autowired
	private GenericDao<GeneralCommentEntity> daoComment;
	@Autowired
	private GenericDao<GeneralLikeEntity> daoGeneralLike;

	@Autowired
	private GenericDao<GeneralCommentEntity> daoGeneralComment;
	@Autowired
	private GenericDao<GeneralPublicationEntity> daoGeneralPublication;

	@Autowired
	private GenericDao<TeamRequestEntity> daoTeamRequest;

	@Autowired
	private GenericDao<TeamAdminEntity> daoTeamAdmin;

	@Autowired
	private GenericDao<TeamEntity> daoTeam;

	@Autowired
	private GenericDao<TeamProfileEntity> daoTeamProfile;

	@Autowired
	private GenericDao<TeamPublicationEntity> daoTeamPublication;

	@Autowired
	private GenericDao<InvitationEntity> daoInvitation;

	@Autowired
	private GenericDao<ProfileEntity> daoProfile;

	@Autowired
	private GenericDao<FriendEntity> daoFriend;
	@Autowired
	private GenericDao<UserEntity> daoUser;

	@Autowired
	private GenericDao<ProfileSteamEntity> daoSteam;

	@Autowired
	private UserEntity userEntity;

	@Autowired
	private ProfileEntity profile;

	@Autowired
	private List<GeneralCommentEntity> listComment;

	@Autowired
	private ProfileTwitchEntity profileTwitch;
	@Autowired
	private GenericDao<ProfileTwitchEntity> daoTwitch;

	@RequestMapping(value = "/view/viewComments/{idPub}", method = RequestMethod.GET)
	public void viewComments(@PathVariable(value = "idPub") String idPub, HttpServletRequest request,
			HttpServletResponse response, ModelAndView model) throws IOException {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("publication_id", Integer.parseInt(idPub));
		listComment = daoComment.listarProperty(GeneralCommentEntity.class, map, "");
		Collections.sort(listComment);
		Collections.reverse(listComment);
		String json = new Gson().toJson(listComment);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(json);

	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view(HttpServletRequest request, ModelAndView model) {

		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Integer invitacao = daoFriend.count2Properties("InvitationEntity", "invited.id", profile.getId().toString(),
				"status", "open");

		Map<String, Object> mapTwitch = new HashMap<String, Object>();
		mapTwitch.put("profile.id", profile.getId());
		if (daoTwitch.exist(ProfileTwitchEntity.class, mapTwitch, "")) {
			profileTwitch = daoTwitch.findByProperty(ProfileTwitchEntity.class, mapTwitch, "");
			model.addObject("twitch", "yes");
			model.addObject("profileTwitch", profileTwitch);

		} else {
			model.addObject("twitch", "no");
		}

		List<GeneralPublicationEntity> listaPublication = new ArrayList<GeneralPublicationEntity>();
		List<GeneralPublicationEntity> listap1 = new ArrayList<GeneralPublicationEntity>();
		List<GeneralPublicationEntity> listap3 = new ArrayList<GeneralPublicationEntity>();

		Map<String, Object> mapPub = new HashMap<String, Object>();
		mapPub.put("publisher.id", profile.getId());

		listaPublication = daoPublication.listarProperty(GeneralPublicationEntity.class, mapPub, "or");
		for (GeneralPublicationEntity gp : listaPublication) {
			if (gp.getShared() == null) {
				gp.setShared("no");

			}
			if (gp.getShared().equals("no")) {
				listap1.add(gp);
			}

		}
		Map<String, Object> mapPub2 = new HashMap<String, Object>();

		mapPub2.put("sharer.id", profile.getId());

		listap3 = daoPublication.listarProperty(GeneralPublicationEntity.class, mapPub2, "or");
		for (GeneralPublicationEntity gp : listap3) {

			listap1.add(gp);

		}

		Collections.sort(listap1);
		Collections.reverse(listap1);

		List<GeneralPublicationEntity> listPubFinal = new ArrayList<GeneralPublicationEntity>();

		for (GeneralPublicationEntity general : listap1) {
			Integer countShared;

			Integer countComment;
			countComment = daoComment.count("GeneralCommentEntity", "publication.id", general.getId().toString());

			general.setCountComment(countComment);

			countShared = daoPublication.count2Properties("GeneralPublicationEntity", "shared", "yes", "originalID",
					general.getId().toString());
			general.setCountShared(countShared);

			Integer countLike;
			countLike = daoLike.count("GeneralLikeEntity", "publication.id", general.getId().toString());

			general.setCountLike(countLike);

			Map<String, Object> mapLike = new HashMap<String, Object>();
			mapLike.put("publication.id", general.getId());
			mapLike.put("liker.id", profile.getId());

			if (daoLike.exist(GeneralLikeEntity.class, mapLike, "and")) {
				general.setYouLiked("yes");
			} else {
				general.setYouLiked("no");
			}

			listPubFinal.add(general);
		}
		Map<String, Object> mapSteam = new HashMap<String, Object>();
		mapSteam.put("profile.id", profile.getId());
		if (daoSteam.exist(ProfileSteamEntity.class, mapSteam, "and")) {
			model.addObject("steam", "yes");
		} else {
			model.addObject("steam", "no");
		}

		Integer count = 0;
		count = daoFriend.count("FriendEntity", "profile1.id", profile.getId().toString())
				+ daoFriend.count("FriendEntity", "profile2.id", profile.getId().toString());
		model.addObject("friends", count);
		model.addObject("listaP", listPubFinal);

		model.addObject("invitacao", invitacao);

		model.addObject("profile", profile);
		model.addObject("profileLoged", profile);
		model.setViewName("profile/view");

		return model;

	}

	@RequestMapping(value = { "/register" }, method = RequestMethod.GET)
	public String register(HttpServletRequest request, ModelAndView model) {
		return "profile/register";
	}

	@RequestMapping(value = "/savephoto", method = RequestMethod.POST)
	public ModelAndView savephoto(MultipartFile file, HttpServletRequest request, ProfileEntity profile,
			ModelAndView model, HttpSession session) throws IllegalStateException, IOException {

		if (profile.getId() != null) {
			profile = daoProfile.buscaId(ProfileEntity.class, profile.getId());
			String photo = profile.getPhoto();
			if (file.toString() != null && !file.getOriginalFilename().equals("")) {
				System.out.println("nao eh null");
				File convFile = new File(request.getRealPath("/img/") + file.getOriginalFilename());
				file.transferTo(convFile);

				SimpleDateFormat df = new SimpleDateFormat("ddMMyyyyHHmmss");
				final Calendar cal = Calendar.getInstance();

				String nome = profile.getFirstName().toLowerCase() + profile.getLastName().toLowerCase()
						+ userEntity.getId() + df.format(cal.getTime()) + "profile";

				String foto = DropBoxUtil.uploadFile(convFile, "/" + nome.trim() + ".jpg");
				profile.setPhoto(foto);

				DropBoxUtil.deleteFile(profile.getPhotoName());
				convFile.delete();
			} else {
				profile.setPhoto(null);
			}

			daoProfile.saveUpdate(profile);
			session.setAttribute("profileLogado", profile);

			model.setViewName("redirect:/profile/view");
			return model;
		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/changephoto", method = RequestMethod.GET)
	public ModelAndView changephoto(HttpServletRequest request, ModelAndView model) {

		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		model.addObject("profile", profile);

		model.setViewName("profile/changephoto");

		return model;

	}

	@RequestMapping(value = "/doDelete", method = RequestMethod.GET)
	public ModelAndView doDelete(HttpServletRequest request, ModelAndView model) {

		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");

		Map<String, Object> map5000 = new HashMap<String, Object>();
		map5000.put("user.id", userEntity.getId());

		if (!daoProfile.exist(ProfileEntity.class, map5000, "and")) {
			daoUser.remove(UserEntity.class, userEntity.getId());
			model.setViewName("redirect:/user/logout");
			return model;

		} else {

			profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

			Map<String, Object> mapF = new HashMap<String, Object>();
			mapF.put("profile1.id", profile.getId());
			mapF.put("profile2.id", profile.getId());
			daoFriend.delete(FriendEntity.class, mapF, "or");

			Map<String, Object> mapGL = new HashMap<String, Object>();
			mapGL.put("liker.id", profile.getId());
			daoGeneralLike.delete(GeneralLikeEntity.class, mapGL, "");

			Map<String, Object> mapGC = new HashMap<String, Object>();
			mapGC.put("commenter.id", profile.getId());
			daoGeneralComment.delete(GeneralCommentEntity.class, mapGC, "");

			Map<String, Object> mapGP = new HashMap<String, Object>();
			mapGP.put("publisher.id", profile.getId());
			daoGeneralPublication.delete(GeneralPublicationEntity.class, mapGP, "");

			Map<String, Object> mapI = new HashMap<String, Object>();
			mapI.put("requester.id", profile.getId());
			mapI.put("invited.id", profile.getId());
			daoInvitation.delete(InvitationEntity.class, mapI, "or");

			Map<String, Object> mapPS = new HashMap<String, Object>();
			mapPS.put("profile.id", profile.getId());
			daoSteam.delete(ProfileSteamEntity.class, mapPS, "");

			Map<String, Object> mapTA = new HashMap<String, Object>();
			mapTA.put("admin.id", profile.getId());
			daoTeamAdmin.delete(TeamAdminEntity.class, mapTA, "");

			Map<String, Object> mapT = new HashMap<String, Object>();
			mapT.put("owner.id", profile.getId());
			daoTeam.delete(TeamEntity.class, mapT, "");

			Map<String, Object> mapTP = new HashMap<String, Object>();
			mapTP.put("profile.id", profile.getId());
			daoTeamProfile.delete(TeamProfileEntity.class, mapTP, "");

			Map<String, Object> mapTPu = new HashMap<String, Object>();
			mapTPu.put("publisher.id", profile.getId());
			daoTeamPublication.delete(TeamPublicationEntity.class, mapTPu, "");

			Map<String, Object> mapTR = new HashMap<String, Object>();
			mapTR.put("requester.id", profile.getId());
			daoTeamRequest.delete(TeamRequestEntity.class, mapTR, "");

			daoProfile.remove(ProfileEntity.class, profile.getId());

			daoUser.remove(UserEntity.class, userEntity.getId());
			model.setViewName("redirect:/user/logout");
			return model;

		}

	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(HttpServletRequest request, ModelAndView model) {

		model.setViewName("profile/delete");
		return model;

	}

	@RequestMapping(value = "/editPassword", method = RequestMethod.GET)
	public ModelAndView editPassword(HttpServletRequest request, ModelAndView model) {

		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");

		Map<String, Object> map5000 = new HashMap<String, Object>();
		map5000.put("user.id", userEntity.getId());

		if (!daoProfile.exist(ProfileEntity.class, map5000, "and")) {
			model.setViewName("redirect:/");

			return model;

		} else {

			profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");
			model.addObject("profile", profile);
			model.addObject("userEntity", userEntity);

			model.setViewName("profile/editPassword");

			return model;
		}

	}

	@RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
	public ModelAndView updatePassword(HttpServletRequest request, String oldPassword, String newPassword,
			String reNewPassword, ModelAndView model) {

		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		Map<String, Object> map500 = new HashMap<String, Object>();
		map500.put("email", userEntity.getEmail());
		map500.put("password", Criptografia.criptografar(oldPassword));

		if (daoUser.exist(UserEntity.class, map500, "and")) {

			if (newPassword.equals(reNewPassword)) {
				userEntity.setPassword(Criptografia.criptografar(newPassword));

				daoUser.saveUpdate(userEntity);

				model.setViewName("redirect:/profile/view");
				return model;
			} else {
				model.addObject("erro", "Password must be the same!");
				model.setViewName("profile/editPassword");
				return model;
			}

		} else {

			model.addObject("erro", "Old Password incorrect");
			model.setViewName("profile/editPassword");
			return model;

		}

	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ModelAndView update(HttpServletRequest request, ProfileEntity profile, ModelAndView model) {

		String firstName = profile.getFirstName().trim();
		String lastName = profile.getLastName().trim();
		String about = profile.getAbout();
		String nickName = profile.getNickName();

		profile = daoProfile.buscaId(ProfileEntity.class, profile.getId());

		profile.setFirstName(firstName);
		profile.setLastName(lastName);
		profile.setAbout(about);
		profile.setNickName(nickName);
		daoProfile.saveUpdate(profile);

		model.setViewName("redirect:/profile/view");
		return model;

	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(HttpServletRequest request, ModelAndView model) {

		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");

		Map<String, Object> map5000 = new HashMap<String, Object>();
		map5000.put("user.id", userEntity.getId());

		if (!daoProfile.exist(ProfileEntity.class, map5000, "and")) {
			model.setViewName("redirect:/");

			return model;

		} else {

			profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

			model.addObject("profile", profile);
			model.setViewName("profile/edit");

			return model;
		}

	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView add(MultipartFile file, HttpSession session, HttpServletRequest request, ProfileEntity profile,
			ModelAndView model) throws IllegalStateException, IOException, GeneralSecurityException {

		userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");
		profile.setAbout(profile.getAbout().trim());
		profile.setUser(userEntity);
		if (file != null) {
			System.out.println("nao eh null");
			File convFile = new File(request.getRealPath("/img/") + file.getOriginalFilename());
			file.transferTo(convFile);

			SimpleDateFormat df = new SimpleDateFormat("ddMMyyyyHHmmss");
			final Calendar cal = Calendar.getInstance();

			String nome = profile.getFirstName().toLowerCase() + profile.getLastName().toLowerCase()
					+ userEntity.getId() + df.format(cal.getTime()) + "profile";

			String foto = DropBoxUtil.uploadFile(convFile, "/" + nome.trim() + ".jpg");
			foto = foto.replaceAll("dl=0", "raw=1");
			profile.setPhoto(foto);
			profile.setPhotoName("/" + nome.trim() + ".jpg");
			convFile.delete();

		}
		if (profile.getCountry().equals("Brazil")) {
			profile.setTimeZone("America/Sao_Paulo");
			Locale locale = new Locale("pt", "BR");
			cultura.setLocale(locale);
			session.setAttribute("cultura", cultura);
		}
		if (profile.getCountry().equals("United States")) {
			profile.setTimeZone("America/New_York");
			Locale locale = new Locale("en", "US");
			cultura.setLocale(locale);
			session.setAttribute("cultura", cultura);
		}

		daoProfile.saveUpdate(profile);

		Map<String, Object> map500 = new HashMap<String, Object>();
		map500.put("user.id", userEntity.getId());
		profile = daoProfile.findByProperty(ProfileEntity.class, map500, "and");

		session.setAttribute("idProfile", profile.getId());
		session.setAttribute("profileLogado", profile);
		model.setViewName("redirect:/profile/view");

		return model;

	}

}
