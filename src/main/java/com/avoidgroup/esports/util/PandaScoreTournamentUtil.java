package com.avoidgroup.esports.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONArray;
import org.json.JSONException;

public class PandaScoreTournamentUtil {
	private static final String ACCESS_TOKEN = "sL3PReaZ8uJPnbKFbMrIEukZ7rOxpaEQ-xmPRwNkHl_sBfB9cLc";

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	private static JSONArray readJsonFromUrl(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			// JSONObject json = new JSONObject(jsonText);
			JSONArray values = new JSONArray(jsonText);
			return values;
		} finally {
			is.close();
		}
	}

	public JSONArray GetTournaments() {

		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/tournaments?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}

	public JSONArray GetPastTournaments() {

		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/tournaments/past?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}

	public JSONArray GetRunningTournaments() {

		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/tournaments/running?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}

	public JSONArray GetUpComingTournaments() {

		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/tournaments/upcoming?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}

	public JSONArray GetATournament(String tournamentID) {

		try {

			JSONArray values = readJsonFromUrl("https://api.pandascore.co/tournaments/" + tournamentID
					+ "?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}

	public JSONArray GetATournamentBrackets(String tournamentID) {

		try {

			JSONArray values = readJsonFromUrl("https://api.pandascore.co/tournaments/" + tournamentID
					+ "/brackets?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}

	public JSONArray GetMatchesForATournament(String tournamentID) {

		try {

			JSONArray values = readJsonFromUrl("https://api.pandascore.co/tournaments/" + tournamentID
					+ "/matches?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}

	public JSONArray GetTournamentStanding(String tournamentID) {

		try {

			JSONArray values = readJsonFromUrl("https://api.pandascore.co/tournaments/" + tournamentID
					+ "/standings?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}

	public JSONArray GetTeamsForATournament(String tournamentID) {

		try {

			JSONArray values = readJsonFromUrl("https://api.pandascore.co/tournaments/" + tournamentID
					+ "/teams?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}

}
