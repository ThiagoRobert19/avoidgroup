package com.avoidgroup.tournaments.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.avoidgroup.team.model.TeamEntity;
import com.avoidgroup.util.EntidadeBase;


@Component("ProfessionalTournamentEntity")
@Entity
public class ProfessionalTournamentEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;
	
	private Integer lineUpCount;
	private String matchstatus;
	private String description;
	private Integer max_allowed;
	private String type;
	private Integer numberOfGame;
	@Temporal(TemporalType.DATE)
	private Date dateOfPublication;

	@Temporal(TemporalType.DATE)
	private Date begin_date;

	@DateTimeFormat(pattern = "HH:mm:ss")
	@Temporal(TemporalType.TIME)
	private Date begin_hour;

	@Temporal(TemporalType.DATE)
	private Date end_date;

	

	@OneToOne
	@JoinColumn(name = "game_id")
	private GameEntity game;

	private String image_default;

	private String image_default_name;

	@OneToOne
	@JoinColumn(name = "winner_id")
	private TeamEntity winner;

	@Column(name = "prize", precision = 19, scale = 2)
	private BigDecimal prize;

	

	@OneToOne
	@JoinColumn(name = "season_id")
	private ProfessionalSeasonEntity season;

	@OneToOne
	@JoinColumn(name = "league_id")
	private ProfessionalLeagueEntity league;

	

	public Integer getId() {
		return id;
	}

	public String getMatchstatus() {
		return matchstatus;
	}

	public void setMatchstatus(String matchstatus) {
		this.matchstatus = matchstatus;
	}

	public Integer getNumberOfGame() {
		return numberOfGame;
	}

	public void setNumberOfGame(Integer numberOfGame) {
		this.numberOfGame = numberOfGame;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLineUpCount() {
		return lineUpCount;
	}

	public void setLineUpCount(Integer lineUpCount) {
		this.lineUpCount = lineUpCount;
	}

	public Date getDateOfPublication() {
		return dateOfPublication;
	}

	public void setDateOfPublication(Date dateOfPublication) {
		this.dateOfPublication = dateOfPublication;
	}

	public Date getBegin_date() {
		return begin_date;
	}

	public void setBegin_date(Date begin_date) {
		this.begin_date = begin_date;
	}

	public Date getBegin_hour() {
		return begin_hour;
	}

	public void setBegin_hour(Date begin_hour) {
		this.begin_hour = begin_hour;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getMax_allowed() {
		return max_allowed;
	}

	public void setMax_allowed(Integer max_allowed) {
		this.max_allowed = max_allowed;
	}

	public GameEntity getGame() {
		return game;
	}

	public void setGame(GameEntity game) {
		this.game = game;
	}

	public String getImage_default() {
		return image_default;
	}

	public void setImage_default(String image_default) {
		this.image_default = image_default;
	}

	public String getImage_default_name() {
		return image_default_name;
	}

	public void setImage_default_name(String image_default_name) {
		this.image_default_name = image_default_name;
	}

	public TeamEntity getWinner() {
		return winner;
	}

	public void setWinner(TeamEntity winner) {
		this.winner = winner;
	}

	public BigDecimal getPrize() {
		return prize;
	}

	public void setPrize(BigDecimal prize) {
		this.prize = prize;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ProfessionalSeasonEntity getSeason() {
		return season;
	}

	public void setSeason(ProfessionalSeasonEntity season) {
		this.season = season;
	}

	public ProfessionalLeagueEntity getLeague() {
		return league;
	}

	public void setLeague(ProfessionalLeagueEntity league) {
		this.league = league;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ProfessionalTournamentEntity [id=" + id + ", dateOfPublication=" + dateOfPublication + ", begin_date="
				+ begin_date + ", begin_hour=" + begin_hour + ", end_date=" + end_date + ", name=" + name
				+ ", max_allowed=" + max_allowed + ", game=" + game + ", image_default=" + image_default
				+ ", image_default_name=" + image_default_name + ", winner=" + winner + ", prize=" + prize
				+ ", description=" + description + ", season=" + season + ", league=" + league + ", type=" + type + "]";
	}

	
	

}
