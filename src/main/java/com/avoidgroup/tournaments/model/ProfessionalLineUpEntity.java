package com.avoidgroup.tournaments.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.springframework.stereotype.Component;

import com.avoidgroup.team.model.TeamEntity;
import com.avoidgroup.util.EntidadeBase;


@Component("ProfessionalLineUpEntity")
@Entity
public class ProfessionalLineUpEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@OneToOne
	@JoinColumn(name = "tournament_id")
	private ProfessionalTournamentEntity tournament;

	@OneToOne
	@JoinColumn(name = "team_id")
	private TeamEntity team;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ProfessionalTournamentEntity getTournament() {
		return tournament;
	}

	public void setTournament(ProfessionalTournamentEntity tournament) {
		this.tournament = tournament;
	}

	public TeamEntity getTeam() {
		return team;
	}

	public void setTeam(TeamEntity team) {
		this.team = team;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ProfessionalLineUpEntity [id=" + id + ", tournament=" + tournament + ", team=" + team + "]";
	}

}
