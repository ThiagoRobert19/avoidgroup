package com.avoidgroup.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.avoidgroup.dao.GenericDao;
import com.avoidgroup.model.AboutFeaturedEntity;
import com.avoidgroup.model.FeaturedGamesEntity;
import com.avoidgroup.model.FriendEntity;
import com.avoidgroup.model.GeneralCommentEntity;
import com.avoidgroup.model.GeneralLikeEntity;
import com.avoidgroup.model.GeneralPublicationEntity;
import com.avoidgroup.model.InvitationEntity;
import com.avoidgroup.model.ProfileEntity;
import com.avoidgroup.model.UserEntity;
import com.avoidgroup.steam.model.ProfileSteamEntity;
import com.avoidgroup.twitchapi.model.ProfileTwitchEntity;
import com.avoidgroup.util.Common;

@Controller
@RequestMapping(value = "/friend")
public class FriendController {
	@Autowired
	private GenericDao<GeneralPublicationEntity> daoPublication;

	@Autowired
	private GenericDao<ProfileEntity> daoProfile;

	@Autowired
	private GenericDao<FriendEntity> daoFriend;

	@Autowired
	private GenericDao<GeneralLikeEntity> daoLike;

	@Autowired
	private GenericDao<InvitationEntity> daoInvitation;

	@Autowired
	private UserEntity userEntity;

	@Autowired
	private FriendEntity friend;

	@Autowired
	private ProfileEntity profile;

	@Autowired
	private ProfileEntity profileFriend;

	@Autowired
	private ProfileEntity profile2;

	@Autowired
	private ProfileEntity profileRequest;

	@Autowired
	private InvitationEntity invitation;
	@Autowired
	private GenericDao<ProfileSteamEntity> daoSteam;
	@Autowired
	private ProfileSteamEntity profileSteam;
	@Autowired
	private ProfileTwitchEntity profileTwitch;
	@Autowired
	private GenericDao<ProfileTwitchEntity> daoTwitch;
	@Autowired
	private GenericDao<GeneralCommentEntity> daoComment;

	@RequestMapping(value = { "/viewFriend/{id}" }, method = RequestMethod.GET)
	public ModelAndView view(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {

		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");
		

		Map<String, Object> mapProfile = new HashMap<String, Object>();
		mapProfile.put("id", Integer.parseInt(id));

		if (daoProfile.exist(ProfileEntity.class, mapProfile, "and")
				&& !profile.getId().equals(profileFriend.getId())) {

			profileFriend = daoProfile.buscaId(ProfileEntity.class, Integer.parseInt(id));

			Map<String, Object> mapTwitch = new HashMap<String, Object>();
			mapTwitch.put("profile.id", profileFriend.getId());
			if (daoTwitch.exist(ProfileTwitchEntity.class, mapTwitch, "")) {
				profileTwitch = daoTwitch.findByProperty(ProfileTwitchEntity.class, mapTwitch, "");
				model.addObject("twitch", "yes");
				model.addObject("profileTwitch", profileTwitch);

			} else {
				model.addObject("twitch", "no");
			}

			Integer quantidade = daoFriend.count("FriendEntity", "profile1.id", profileFriend.getId().toString())
					+ daoFriend.count("FriendEntity", "profile2.id", profileFriend.getId().toString());

			Map<String, Object> map101 = new HashMap<String, Object>();
			map101.put("profile1.id", profileFriend.getId());
			map101.put("profile2.id", profile.getId());

			Map<String, Object> map201 = new HashMap<String, Object>();
			map201.put("profile1.id", profile.getId());
			map201.put("profile2.id", profileFriend.getId());

			// ===============================================Publica�oes================================
			List<GeneralPublicationEntity> listaPublication = new ArrayList<GeneralPublicationEntity>();
			List<GeneralPublicationEntity> listap1 = new ArrayList<GeneralPublicationEntity>();
			List<GeneralPublicationEntity> listap3 = new ArrayList<GeneralPublicationEntity>();

			Map<String, Object> mapPub = new HashMap<String, Object>();
			mapPub.put("publisher.id", profileFriend.getId());

			listaPublication = daoPublication.listarProperty(GeneralPublicationEntity.class, mapPub, "or");
			for (GeneralPublicationEntity gp : listaPublication) {
				if (gp.getShared() == null) {
					gp.setShared("no");

				}
				if (gp.getShared().equals("no")) {
					listap1.add(gp);
				}

			}
			Map<String, Object> mapPub2 = new HashMap<String, Object>();

			mapPub2.put("sharer.id", profileFriend.getId());

			listap3 = daoPublication.listarProperty(GeneralPublicationEntity.class, mapPub2, "or");
			for (GeneralPublicationEntity gp : listap3) {

				listap1.add(gp);

			}

			Collections.sort(listap1);
			Collections.reverse(listap1);

			List<GeneralPublicationEntity> listPubFinal = new ArrayList<GeneralPublicationEntity>();

			for (GeneralPublicationEntity general : listap1) {
				Integer countShared;
				countShared = daoPublication.count2Properties("GeneralPublicationEntity", "shared", "yes", "originalID",
						general.getId().toString());
				general.setCountShared(countShared);

				Integer countComment;
				countComment = daoComment.count("GeneralCommentEntity", "publication.id", general.getId().toString());

				general.setCountComment(countComment);
				Integer countLike;
				countLike = daoLike.count("GeneralLikeEntity", "publication.id", general.getId().toString());

				general.setCountLike(countLike);

				Map<String, Object> mapLike = new HashMap<String, Object>();
				mapLike.put("publication.id", general.getId());
				mapLike.put("liker.id", profile.getId());

				if (daoLike.exist(GeneralLikeEntity.class, mapLike, "and")) {
					general.setYouLiked("yes");
				} else {
					general.setYouLiked("no");
				}

				listPubFinal.add(general);
			}

			model.addObject("listaP", listPubFinal);
			Integer count = 0;
			count = daoFriend.count("FriendEntity", "profile1.id", profileFriend.getId().toString())
					+ daoFriend.count("FriendEntity", "profile2.id", profileFriend.getId().toString());
			model.addObject("friends", count);

			Map<String, Object> mapSteam = new HashMap<String, Object>();
			mapSteam.put("profile.id", profileFriend.getId());
			if (daoSteam.exist(ProfileSteamEntity.class, mapSteam, "and")) {
				model.addObject("steam", "yes");
			} else {
				model.addObject("steam", "no");
			}

			if ((daoFriend.exist(FriendEntity.class, map101, "and"))
					|| (daoFriend.exist(FriendEntity.class, map201, "and"))) {

				model.addObject("profile", profileFriend);
				model.addObject("quantidade", quantidade);

				model.setViewName("friend/viewAsYourFriend");
				return model;
			} else {

				Map<String, Object> mapInvitation = new HashMap<String, Object>();
				mapInvitation.put("requester.id", profile.getId());
				mapInvitation.put("invited.id", profileFriend.getId());

				if (daoInvitation.exist(InvitationEntity.class, mapInvitation, "and")) {
					model.addObject("resp", "esperando");
				}
				Map<String, Object> mapInvited = new HashMap<String, Object>();
				mapInvited.put("invited.id", profile.getId());
				mapInvited.put("requester.id", profileFriend.getId());

				if (daoInvitation.exist(InvitationEntity.class, mapInvited, "and")) {
					model.addObject("resp", "invitado");
				}
				if ((!daoInvitation.exist(InvitationEntity.class, mapInvitation, "and"))
						&& (!daoInvitation.exist(InvitationEntity.class, mapInvited, "and"))) {

					model.addObject("resp", "nada");
				}
				model.addObject("profile", profileFriend);

				model.addObject("quantidade", quantidade);

				model.setViewName("friend/viewFriend");
				return model;
			}
		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = { "/delete/{id}" }, method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {

		Map<String, Object> mapProfile = new HashMap<String, Object>();
		mapProfile.put("id", Integer.parseInt(id));

		if (daoProfile.exist(ProfileEntity.class, mapProfile, "and")) {

			profileFriend = daoProfile.buscaId(ProfileEntity.class, Integer.parseInt(id));

			profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

			Map<String, Object> mapP1 = new HashMap<String, Object>();
			mapP1.put("profile1.id", profile.getId());
			mapP1.put("profile2.id", profileFriend.getId());

			Map<String, Object> mapP2 = new HashMap<String, Object>();
			mapP2.put("profile1.id", profileFriend.getId());
			mapP2.put("profile2.id", profile.getId());

			if ((!daoFriend.exist(FriendEntity.class, mapP1, "and"))
					&& (!daoFriend.exist(FriendEntity.class, mapP2, "and"))) {

				model.setViewName("redirect:/friend/viewFriend/" + id + "");
				return model;

			} else {

				if (daoFriend.exist(FriendEntity.class, mapP1, "and")) {
					daoFriend.delete(FriendEntity.class, mapP1, "and");
				}

				if (daoFriend.exist(FriendEntity.class, mapP2, "and")) {

					daoFriend.delete(FriendEntity.class, mapP2, "and");

				}
				model.setViewName("redirect:/friend/viewFriend/" + id + "");
				return model;
			}
		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/viewFriendList/{id}", method = RequestMethod.GET)
	public ModelAndView viewFriendList(@PathVariable(value = "id") String id, String name, HttpServletRequest request,
			ModelAndView model) {

		Map<String, Object> mapProfile = new HashMap<String, Object>();
		mapProfile.put("id", Integer.parseInt(id));

		if (daoProfile.exist(ProfileEntity.class, mapProfile, "and")) {
			profile = daoProfile.buscaId(ProfileEntity.class, Integer.parseInt(id));

			List<FriendEntity> lista = new ArrayList<FriendEntity>();

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("profile1.id", profile.getId());
			map.put("profile2.id", profile.getId());

			lista = daoFriend.listarProperty(FriendEntity.class, map, "or");

			List<ProfileEntity> listaProfile = new ArrayList<ProfileEntity>();
			profile2 = (ProfileEntity) request.getSession().getAttribute("profileLogado");

			for (FriendEntity f : lista) {

				if (f.getProfile1().getId().equals(profile.getId())) {

					listaProfile.add(f.getProfile2());
				}
				if (f.getProfile2().getId().equals(profile.getId())) {

					listaProfile.add(f.getProfile1());
				}

			}

			model.setViewName("friend/viewFriendList");
			model.addObject("profile", profile);

			model.addObject("lista", listaProfile);

			return model;
		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/accept/{id}", method = RequestMethod.GET)
	public ModelAndView accept(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {
		Map<String, Object> mapProfile = new HashMap<String, Object>();
		mapProfile.put("id", Integer.parseInt(id));
		if (daoProfile.exist(ProfileEntity.class, mapProfile, "and")) {

			profileRequest = daoProfile.buscaId(ProfileEntity.class, Integer.parseInt(id));

			profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");
			if (!profile.getId().equals(profileRequest.getId())) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("requester.id", profileRequest.getId());
				map.put("invited.id", profile.getId());

				if (daoInvitation.exist(InvitationEntity.class, map, "and")) {
					daoInvitation.delete(InvitationEntity.class, map, "and");

					Calendar date = Calendar.getInstance();

					friend.setProfile1(profileRequest);
					friend.setProfile2(profile);
					friend.setTimeOfFriendship(date.getTime());
					friend.setDateOfFriendship(date.getTime());

					daoFriend.saveUpdate(friend);
					model.setViewName("redirect:/friend/viewFriend/" + id);

					return model;
				} else {
					model.setViewName("redirect:/friend/viewFriend/" + id);

					return model;
				}

			} else {
				model.setViewName("redirect:/");
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/deny/{id}", method = RequestMethod.GET)
	public ModelAndView deny(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {

		Map<String, Object> mapProfile = new HashMap<String, Object>();
		mapProfile.put("id", Integer.parseInt(id));
		if (daoProfile.exist(ProfileEntity.class, mapProfile, "and")) {

			profileRequest = daoProfile.buscaId(ProfileEntity.class, Integer.parseInt(id));

			profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

			if (!profile.getId().equals(profileRequest.getId())) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("requester.id", profileRequest.getId());
				map.put("invited.id", profile.getId());

				if (daoInvitation.exist(InvitationEntity.class, map, "and")) {

					daoInvitation.delete(InvitationEntity.class, map, "and");

					model.setViewName("redirect:/friend/viewFriend/" + id);

					return model;
				} else {
					model.setViewName("redirect:/friend/viewFriend/" + id);

					return model;
				}

			} else {
				model.setViewName("redirect:/");
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}
	}

	@RequestMapping(value = "/find", method = RequestMethod.POST)
	public ModelAndView find(String name, HttpServletRequest request, ModelAndView model) {

		List<ProfileEntity> lista = new ArrayList<ProfileEntity>();

		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("firstName", name);
		map.put("lastName", name);
		map.put("nickName", name);

		lista = daoProfile.listBy3PropertyLike(ProfileEntity.class, "ProfileEntity", "firstName", "lastName",
				"nickName", name);

		List<ProfileEntity> lista2 = new ArrayList<ProfileEntity>();
		for (ProfileEntity p : lista) {
			if (p.getId() != profile.getId()) {
				lista2.add(p);
			}
		}
		model.setViewName("friend/find");
		model.addObject("lista", lista2);

		return model;

	}

	@RequestMapping(value = "/viewYourList", method = RequestMethod.GET)
	public ModelAndView viewYourList(String name, HttpServletRequest request, ModelAndView model) {

		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		List<FriendEntity> lista = new ArrayList<FriendEntity>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("profile1.id", profile.getId());
		map.put("profile2.id", profile.getId());

		lista = daoFriend.listarProperty(FriendEntity.class, map, "or");

		List<ProfileEntity> listaProfile = new ArrayList<ProfileEntity>();

		for (FriendEntity f : lista) {

			if (f.getProfile1().getId().equals(profile.getId())) {
				listaProfile.add(f.getProfile2());
			} else {
				listaProfile.add(f.getProfile1());
			}
		}
		model.setViewName("friend/viewYourList");
		model.addObject("lista", listaProfile);

		return model;

	}

	@RequestMapping(value = "/add/{id}", method = RequestMethod.GET)
	public ModelAndView add(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));

		if (daoProfile.exist(ProfileEntity.class, map, "and")) {

			profile = daoProfile.buscaId(ProfileEntity.class, Integer.parseInt(id));

			profileRequest = (ProfileEntity) request.getSession().getAttribute("profileLogado");

			Map<String, Object> mapFriend1 = new HashMap<String, Object>();
			mapFriend1.put("profile1.id", profileRequest.getId().toString());
			mapFriend1.put("profile2.id", profile.getId().toString());

			Map<String, Object> mapFriend2 = new HashMap<String, Object>();
			mapFriend2.put("profile2.id", profileRequest.getId().toString());
			mapFriend2.put("profile1.id", profile.getId().toString());

			if (!profileRequest.getId().equals(profile.getId())
					&& (!daoFriend.exist(FriendEntity.class, mapFriend1, "and")
							&& !daoFriend.exist(FriendEntity.class, mapFriend2, "and"))) {

				InvitationEntity invite = new InvitationEntity();
				invite.setRequester(profileRequest);
				invite.setInvited(profile);
				invite.setStatus("open");

				Calendar date1 = Calendar.getInstance();
				invite.setData(date1.getTime());

				daoInvitation.saveUpdate(invite);

				model.setViewName("redirect:/friend/viewFriend/" + id + "");
				return model;

			} else {
				model.setViewName("redirect:/");
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/cancel/{id}", method = RequestMethod.GET)
	public ModelAndView cancel(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));

		if (daoProfile.exist(ProfileEntity.class, map, "and")) {
			profile = daoProfile.buscaId(ProfileEntity.class, Integer.parseInt(id));
			profileRequest = (ProfileEntity) request.getSession().getAttribute("profileLogado");

			Map<String, Object> mapFriend1 = new HashMap<String, Object>();
			mapFriend1.put("profile1.id", profileRequest.getId().toString());
			mapFriend1.put("profile2.id", profile.getId().toString());

			Map<String, Object> mapFriend2 = new HashMap<String, Object>();
			mapFriend2.put("profile2.id", profileRequest.getId().toString());
			mapFriend2.put("profile1.id", profile.getId().toString());

			if (!profileRequest.getId().equals(profile.getId())
					&& (!daoFriend.exist(FriendEntity.class, mapFriend1, "and")
							&& !daoFriend.exist(FriendEntity.class, mapFriend2, "and"))) {

				Map<String, Object> mapRequest = new HashMap<String, Object>();
				mapRequest.put("requester.id", profileRequest.getId());
				mapRequest.put("invited.id", profile.getId());
				daoInvitation.delete(InvitationEntity.class, mapRequest, "and");

				model.setViewName("redirect:/friend/viewFriend/" + id);
				return model;

			} else {
				model.setViewName("redirect:/");
				return model;
			}
		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

}
