package com.avoidgroup.team.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.avoidgroup.amateur.tournament.model.AmateurLineUpEntity;
import com.avoidgroup.amateur.tournament.model.AmateurMatchEntity;
import com.avoidgroup.amateur.tournament.model.AmateurTournamentEntity;
import com.avoidgroup.amateur.tournament.model.AmateurTournamentInvitationEntity;
import com.avoidgroup.dao.GenericDao;
import com.avoidgroup.model.ProfileEntity;
import com.avoidgroup.model.UserEntity;
import com.avoidgroup.team.model.TeamAdminEntity;
import com.avoidgroup.team.model.TeamEntity;
import com.avoidgroup.team.model.TeamEventEntity;
import com.avoidgroup.team.model.TeamFaqEntity;
import com.avoidgroup.team.model.TeamProfileEntity;
import com.avoidgroup.team.model.TeamPublicationEntity;
import com.avoidgroup.team.model.TeamQuestEntity;
import com.avoidgroup.team.model.TeamRequestEntity;
import com.avoidgroup.team.model.TeamTrainingEntity;
import com.avoidgroup.util.Common;
import com.avoidgroup.util.DropBoxUtil;
import com.avoidgroup.tournaments.model.GameEntity;

@Controller
@RequestMapping(value = "/team")
public class TeamController {
	@Autowired
	private GenericDao<AmateurLineUpEntity> daoLineUp;

	@Autowired
	private GenericDao<AmateurTournamentEntity> daoTournament;

	@Autowired
	private GenericDao<AmateurTournamentInvitationEntity> daoTournamentInvitation;

	@Autowired
	private GenericDao<AmateurMatchEntity> daoTournamentMatch;

	@Autowired
	private GenericDao<TeamEntity> daoTeam;

	@Autowired
	private GenericDao<ProfileEntity> daoProfile;

	@Autowired
	private GenericDao<TeamRequestEntity> daoTeamRequest;

	@Autowired
	private GenericDao<TeamProfileEntity> daoTeamProfile;

	@Autowired
	private GenericDao<TeamAdminEntity> daoTeamAdmin;

	@Autowired
	private GenericDao<TeamPublicationEntity> daoTeamPublication;

	@Autowired
	private GenericDao<GameEntity> daoGameEntity;

	@Autowired
	private ProfileEntity profile;

	@Autowired
	private ProfileEntity profile2;

	@Autowired
	private TeamEntity team;

	@Autowired
	private TeamRequestEntity teamRequest;

	@Autowired
	private List<TeamProfileEntity> listTeamProfile;
	@Autowired
	private TeamAdminEntity adminEntity;

	@Autowired
	private GenericDao<TeamEventEntity> daoEvent;

	@Autowired
	private GenericDao<TeamFaqEntity> daoFaq;

	@Autowired
	private GenericDao<TeamQuestEntity> daoQuest;
	@Autowired
	private GenericDao<TeamTrainingEntity> daoTraining;

	@Autowired
	private GenericDao<AmateurTournamentInvitationEntity> daoInvitation;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView add(HttpServletRequest request, TeamEntity teamEntity, ModelAndView model, String gameID,
			MultipartFile file) throws IllegalStateException, IOException {
		String uuid = UUID.randomUUID().toString().replace("-", "");

		String name = teamEntity.getName();
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		GameEntity game = daoGameEntity.buscaId(GameEntity.class, Integer.parseInt(gameID));

		teamEntity.setOwner(profile);
		teamEntity.setGame(game);
		teamEntity.setUuid(uuid);

		if (file.toString() != null && !file.getOriginalFilename().equals("")) {
			System.out.println("nao eh null");
			File convFile = new File(request.getRealPath("/img/") + file.getOriginalFilename());

			file.transferTo(convFile);

			SimpleDateFormat df = new SimpleDateFormat("ddMMyyyyHHmmss");
			final Calendar cal = Calendar.getInstance();

			String nome = teamEntity.getName().toLowerCase() + df.format(cal.getTime()) + "teamPhoto";

			String foto = DropBoxUtil.uploadFile(convFile, "/" + nome.trim() + ".jpg");

			teamEntity.setImage(foto);

		} else {
			teamEntity.setImage(null);

		}
		System.out.println("Team montado: " + teamEntity.toString());
		model.addObject("profileLoged", profile);
		daoTeam.saveUpdate(teamEntity);
		System.out.println("Time salvo");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", name);
		map.put("owner.id", profile.getId());
		map.put("uuid", uuid);
		System.out.println("Pesquisar o time");
		TeamEntity t = new TeamEntity();

		t = daoTeam.findByProperty(TeamEntity.class, map, "and");
		System.out.println("Time encontrado: " + t.toString());

		TeamProfileEntity tp = new TeamProfileEntity();
		tp.setProfile(profile);
		tp.setTeam(t);

		System.out.println("Time Perfil adicionar: " + tp.toString());
		daoTeamProfile.saveUpdate(tp);

		model.setViewName("redirect:/team/findAll");
		return model;

	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {

		Map<String, Object> mapTeam = new HashMap<String, Object>();
		mapTeam.put("id", Integer.parseInt(id));

		if (daoTeam.exist(TeamEntity.class, mapTeam, "and")) {
			team = daoTeam.buscaId(TeamEntity.class, Integer.parseInt(id));

			profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

			if (team.getOwner().getId().equals(profile.getId())) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("team.id", team.getId());

				daoTeamPublication.delete(TeamPublicationEntity.class, map, "and");

				daoTeamAdmin.delete(TeamAdminEntity.class, map, "and");
				daoTeamProfile.delete(TeamProfileEntity.class, map, "and");
				daoTeamRequest.delete(TeamRequestEntity.class, map, "and");
				daoEvent.delete(TeamEventEntity.class, map, "and");
				daoFaq.delete(TeamFaqEntity.class, map, "and");
				daoQuest.delete(TeamQuestEntity.class, map, "and");
				daoTraining.delete(TeamTrainingEntity.class, map, "and");
				daoLineUp.delete(AmateurLineUpEntity.class, map, "and");

				Map<String, Object> mapTour = new HashMap<String, Object>();
				mapTour.put("creator.id", team.getId());
				daoTournament.delete(AmateurTournamentEntity.class, mapTour, "and");

				Map<String, Object> mapInvitation = new HashMap<String, Object>();
				mapInvitation.put("invited.id", team.getId());
				daoTournamentInvitation.delete(AmateurTournamentInvitationEntity.class, mapInvitation, "and");

				Map<String, Object> mapMatch = new HashMap<String, Object>();
				mapMatch.put("team1.id", team.getId());
				mapMatch.put("team2.id", team.getId());

				daoTournamentMatch.delete(AmateurMatchEntity.class, mapMatch, "or");

				daoTeam.remove(TeamEntity.class, Integer.parseInt(id));
				model.setViewName("redirect:/team/findAll");
				return model;
			} else {

				model.setViewName("redirect:/team/viewTeam/" + id);
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ModelAndView updateName(HttpServletRequest request, TeamEntity team, ModelAndView model) {
		String name = team.getName().trim();
		String about = team.getAbout().trim();
		String id = team.getId().toString();
		team = daoTeam.buscaId(TeamEntity.class, team.getId());
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");
		if (team.getOwner().getId().equals(profile.getId())) {
			team.setName(name);
			team.setAbout(about);
			daoTeam.saveUpdate(team);
			model.setViewName("redirect:/team/viewTeam/" + id);
			return model;
		} else {

			model.setViewName("redirect:/team/viewTeam/" + id);
			return model;
		}

	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {
		Map<String, Object> mapTeam = new HashMap<String, Object>();
		mapTeam.put("id", Integer.parseInt(id));

		if (daoTeam.exist(TeamEntity.class, mapTeam, "and")) {

			team = daoTeam.buscaId(TeamEntity.class, Integer.parseInt(id));

			profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

			Map<String, Object> mapTeamAdmin = new HashMap<String, Object>();
			mapTeamAdmin.put("admin.id", profile.getId());
			mapTeamAdmin.put("team.id", team.getId());

			if (team.getOwner().getId().equals(profile.getId())
					|| (daoTeamAdmin.exist(TeamAdminEntity.class, mapTeamAdmin, "and"))) {

				model.addObject("team", team);
				model.setViewName("team/edit");

				return model;

			} else {

				model.setViewName("redirect:/");

				return model;
			}
		} else {

			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/viewTeam/{id}", method = RequestMethod.GET)
	public ModelAndView view(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {

		Map<String, Object> mapTeam = new HashMap<String, Object>();
		mapTeam.put("id", Integer.parseInt(id));

		if (daoTeam.exist(TeamEntity.class, mapTeam, "and")) {

			team = daoTeam.buscaId(TeamEntity.class, Integer.parseInt(id));

			Integer quantidade = daoTeamProfile.count("TeamProfileEntity", "team.id", team.getId().toString());

			profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

			String cargo = "noGroup";

			Map<String, Object> mapTeamProfile = new HashMap<String, Object>();
			mapTeamProfile.put("profile.id", profile.getId());

			if (daoTeamProfile.exist(TeamProfileEntity.class, mapTeamProfile, "and")) {

				cargo = "group";
				model.addObject("aguardando", "none");
				Integer solicitation = daoTeamRequest.count2Properties("TeamRequestEntity", "team.id",
						team.getId().toString(), "status", "open");

				List<TeamPublicationEntity> lista = new ArrayList<TeamPublicationEntity>();

				Map<String, Object> mappublication = new HashMap<String, Object>();
				mappublication.put("team.id", team.getId());

				lista = daoTeamPublication.listarProperty(TeamPublicationEntity.class, mappublication, "and");
				Collections.reverse(lista);

				Map<String, Object> mapFaq = new HashMap<String, Object>();
				mapFaq.put("team.id", profile.getId());
				mapFaq.put("status", "open");

				Integer faq = daoFaq.count2Properties("TeamFaqEntity", "team.id", team.getId().toString(), "status",
						"open");

				Integer requestT = daoInvitation.count2Properties("AmateurTournamentInvitationEntity", "invited.id",
						team.getId().toString(), "status", "requested");
				model.addObject("faq", faq);
				model.addObject("lista", lista);
				model.addObject("solicitation", solicitation);
				model.addObject("requestT", requestT);

				Map<String, Object> mapTeamAdmin = new HashMap<String, Object>();
				mapTeamAdmin.put("admin.id", profile.getId());

				if (daoTeamAdmin.exist(TeamAdminEntity.class, mapTeamAdmin, "and")) {

					cargo = "admin";
				}
				if (team.getOwner().getId().equals(profile.getId())) {
					cargo = "owner";
				}
			} else {
				Map<String, Object> mapRequest = new HashMap<String, Object>();
				mapRequest.put("requester.id", profile.getId());
				mapRequest.put("status", "open");

				if (daoTeamRequest.exist(TeamRequestEntity.class, mapRequest, "and")) {
					model.addObject("aguardando", "aguardando");
				}
			}

			model.setViewName("team/viewTeam");

			model.addObject("quantidade", quantidade);
			model.addObject("team", team);
			model.addObject("cargo", cargo);

			return model;
		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/denyrequest/{id}", method = RequestMethod.GET)
	public ModelAndView denyrequest(@PathVariable(value = "id") String id, ModelAndView model,
			HttpServletRequest request) {

		Map<String, Object> mapRequest = new HashMap<String, Object>();
		mapRequest.put("id", Integer.parseInt(id));

		if (daoTeamRequest.exist(TeamRequestEntity.class, mapRequest, "and")) {

			teamRequest = daoTeamRequest.buscaId(TeamRequestEntity.class, Integer.parseInt(id));

			profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

			Map<String, Object> mapTeamAdmin = new HashMap<String, Object>();
			mapTeamAdmin.put("admin.id", profile.getId());
			mapTeamAdmin.put("team.id", teamRequest.getTeam().getId());

			if (teamRequest.getTeam().getOwner().getId().equals(profile.getId())
					|| daoTeamAdmin.exist(TeamAdminEntity.class, mapTeamAdmin, "and")) {

				model.setViewName("redirect:/team/viewTeam/" + teamRequest.getTeam().getId() + "");
				daoTeamRequest.remove(TeamRequestEntity.class, teamRequest.getId());

				return model;

			} else {

				model.setViewName("redirect:/team/viewTeam/" + id);
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/acceptrequest/{id}", method = RequestMethod.GET)
	public ModelAndView accept(@PathVariable(value = "id") String id, ModelAndView model, HttpServletRequest request) {

		Map<String, Object> mapRequest = new HashMap<String, Object>();
		mapRequest.put("id", Integer.parseInt(id));

		if (daoTeamRequest.exist(TeamRequestEntity.class, mapRequest, "and")) {

			teamRequest = daoTeamRequest.buscaId(TeamRequestEntity.class, Integer.parseInt(id));

			profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

			Map<String, Object> mapTeamAdmin = new HashMap<String, Object>();
			mapTeamAdmin.put("admin.id", profile.getId());
			mapTeamAdmin.put("team.id", teamRequest.getTeam().getId());

			if (teamRequest.getTeam().getOwner().getId().equals(profile.getId())
					|| daoTeamAdmin.exist(TeamAdminEntity.class, mapTeamAdmin, "and")) {

				TeamProfileEntity teamProfile = new TeamProfileEntity();
				teamProfile.setProfile(teamRequest.getRequester());
				teamProfile.setTeam(teamRequest.getTeam());

				daoTeamProfile.saveUpdate(teamProfile);
				model.setViewName("redirect:/team/viewTeam/" + teamRequest.getTeam().getId() + "");

				daoTeamRequest.remove(TeamRequestEntity.class, teamRequest.getId());

				return model;

			} else {

				model.setViewName("redirect:/team/viewTeam/" + id);
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/viewRequest/{id}", method = RequestMethod.GET)
	public ModelAndView viewRequest(@PathVariable(value = "id") String id, ModelAndView model,
			HttpServletRequest request) {

		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapTeam = new HashMap<String, Object>();
		mapTeam.put("id", Integer.parseInt(id));

		Map<String, Object> mapTeamAdmin = new HashMap<String, Object>();
		mapTeamAdmin.put("admin.id", profile.getId());
		mapTeamAdmin.put("team.id", Integer.parseInt(id));

		if (daoTeam.exist(TeamEntity.class, mapTeam, "and") && (team.getOwner().getId().equals(profile.getId())
				|| daoTeamAdmin.exist(TeamAdminEntity.class, mapTeamAdmin, "and"))) {

			Map<String, Object> mapTeamRequest = new HashMap<String, Object>();
			mapTeamRequest.put("team.id", Integer.parseInt(id));
			mapTeamRequest.put("status", "open");

			List<TeamRequestEntity> list = new ArrayList<TeamRequestEntity>();

			list = daoTeamRequest.listarProperty(TeamRequestEntity.class, mapTeamRequest, "and");

			model.addObject("lista", list);
			model.setViewName("team/viewRequest");

			return model;

		} else {

			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/cancel/{id}", method = RequestMethod.GET)
	public ModelAndView cancel(@PathVariable(value = "id") String id, HttpServletRequest request, ModelAndView model) {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapTeamProfile = new HashMap<String, Object>();
		mapTeamProfile.put("profile.id", profile.getId());
		mapTeamProfile.put("team.id", Integer.parseInt(id));

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("requester.id", profile.getId());
		map.put("team.id", Integer.parseInt(id));
		map.put("status", "open");

		Map<String, Object> mapTeam = new HashMap<String, Object>();
		mapTeam.put("id", Integer.parseInt(id));

		if (daoTeam.exist(TeamEntity.class, mapTeam, "and")
				&& !daoTeamProfile.exist(TeamProfileEntity.class, mapTeamProfile, "and")
				&& daoTeamRequest.exist(TeamRequestEntity.class, map, "and")) {

			daoTeamRequest.delete(TeamRequestEntity.class, map, "and");
			model.setViewName("redirect:/team/viewTeam/" + id);
			return model;
		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/request/{id}", method = RequestMethod.GET)
	public ModelAndView request(@PathVariable(value = "id") String id, HttpServletRequest request, ModelAndView model) {

		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapTeam = new HashMap<String, Object>();
		mapTeam.put("id", Integer.parseInt(id));
		if (daoTeam.exist(TeamEntity.class, mapTeam, "and")) {
			team = daoTeam.buscaId(TeamEntity.class, Integer.parseInt(id));

			Map<String, Object> mapverification = new HashMap<String, Object>();
			mapverification.put("profile.id", profile.getId());
			mapverification.put("team.game.id", team.getGame().getId());

			if (!daoTeamProfile.exist(TeamProfileEntity.class, mapverification, "and")) {
				Map<String, Object> mapTeamProfile = new HashMap<String, Object>();
				mapTeamProfile.put("profile.id", profile.getId());
				mapTeamProfile.put("team.id", Integer.parseInt(id));

				Map<String, Object> map = new HashMap<String, Object>();
				map.put("requester.id", profile.getId());
				map.put("team.id", Integer.parseInt(id));
				map.put("status", "open");

				if (!daoTeamProfile.exist(TeamProfileEntity.class, mapTeamProfile, "and")
						&& !daoTeamRequest.exist(TeamRequestEntity.class, map, "and")) {
					Calendar g = new GregorianCalendar();

					teamRequest.setTeam(team);
					teamRequest.setRequester(profile);
					teamRequest.setDateOfRequest(g.getTime());
					teamRequest.setStatus("open");
					teamRequest.setTimeOfRequest(g.getTime());

					model.setViewName("redirect:/team/viewTeam/" + id + "");
					daoTeamRequest.saveUpdate(teamRequest);
					return model;
				} else {
					model.setViewName("redirect:/");
					return model;
				}

			} else {

				model.addObject("error", "Voc� j� participa de um time com esse Game!");
				model.setViewName("error/erro");

				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView create(ModelAndView model) {

		List<GameEntity> listGames = daoGameEntity.list(GameEntity.class);

		model.addObject("listGames", listGames);

		model.setViewName("team/add");
		return model;

	}

	@RequestMapping(value = "/findAll", method = RequestMethod.GET)
	public ModelAndView findAll(HttpServletRequest request, ModelAndView model) {

		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("profile.id", profile.getId());

		List<TeamProfileEntity> lista = new ArrayList<TeamProfileEntity>();

		lista = daoTeamProfile.listarProperty(TeamProfileEntity.class, map, "and");

		model.setViewName("team/findAll");
		model.addObject("lista", lista);

		return model;

	}

	@RequestMapping(value = "/find", method = RequestMethod.POST)
	public ModelAndView find(String name, HttpServletRequest request, ModelAndView model) {

		List<TeamEntity> lista = new ArrayList<TeamEntity>();

		lista = daoTeam.listByPropertyLike(TeamEntity.class, "TeamEntity", "name", name);
		model.setViewName("team/find");
		model.addObject("lista", lista);

		return model;

	}

	@RequestMapping(value = "/leave/{id}", method = RequestMethod.GET)
	public ModelAndView leave(@PathVariable(value = "id") String id, HttpServletRequest request, ModelAndView model) {

		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapTeam = new HashMap<String, Object>();
		mapTeam.put("id", Integer.parseInt(id));

		Map<String, Object> mapTeamProfile = new HashMap<String, Object>();
		mapTeamProfile.put("team.id", Integer.parseInt(id));
		mapTeamProfile.put("profile.id", profile.getId());

		if (daoTeam.exist(TeamEntity.class, mapTeam, "and")
				&& daoTeamProfile.exist(TeamProfileEntity.class, mapTeamProfile, "and")) {

			daoTeamProfile.delete(TeamProfileEntity.class, mapTeamProfile, "and");

			daoTeamAdmin.delete(TeamAdminEntity.class, mapTeamProfile, "and");

			model.setViewName("redirect:/team/viewTeam/" + id);

			return model;

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/administrator/{id}", method = RequestMethod.GET)
	public ModelAndView administrator(@PathVariable(value = "id") String id, ModelAndView model,
			HttpServletRequest request) {

		Map<String, Object> mapTeam = new HashMap<String, Object>();
		mapTeam.put("id", Integer.parseInt(id));

		if (daoTeam.exist(TeamEntity.class, mapTeam, "")) {
			team = daoTeam.buscaId(TeamEntity.class, Integer.parseInt(id));

			profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

			if (team.getOwner().getId().equals(profile.getId())) {

				List<TeamAdminEntity> lista = new ArrayList<TeamAdminEntity>();

				Map<String, Object> mapTeamAdmin = new HashMap<String, Object>();
				mapTeamAdmin.put("team.id", team.getId());

				lista = daoTeamAdmin.listarProperty(TeamAdminEntity.class, mapTeamAdmin, "");
				model.setViewName("team/administrator");
				model.addObject("lista", lista);
				model.addObject("team", team);

				return model;

			} else {

				model.setViewName("redirect:/team/viewTeam/" + id);
				return model;
			}

		} else {
			model.setViewName("redirect:/team/viewTeam/" + id);
			return model;
		}

	}

	@RequestMapping(value = "/removeAdministrator/{idAdmin}/{idTeam}", method = RequestMethod.GET)
	public ModelAndView removeAdministrator(@PathVariable(value = "idAdmin") String idAdmin,
			@PathVariable(value = "idTeam") String idTeam, ModelAndView model, HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(idTeam));
		if (daoTeam.exist(TeamEntity.class, map, "")) {

			team = daoTeam.buscaId(TeamEntity.class, Integer.parseInt(idTeam));

			profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

			Map<String, Object> mapTeamAdmin = new HashMap<String, Object>();
			mapTeamAdmin.put("team.id", team.getId());
			mapTeamAdmin.put("admin.id", Integer.parseInt(idAdmin));

			if (team.getOwner().getId().equals(profile.getId())
					&& daoTeamAdmin.exist(TeamAdminEntity.class, mapTeamAdmin, "and")) {
				daoTeamAdmin.delete(TeamAdminEntity.class, mapTeamAdmin, "and");

				model.setViewName("redirect:/team/administrator/" + idTeam);
				return model;

			} else {

				model.setViewName("redirect:/team/viewTeam/" + idTeam);
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/createAdministrator/{id}", method = RequestMethod.GET)
	public ModelAndView createAdministrator(@PathVariable(value = "id") String id, ModelAndView model,
			HttpServletRequest request) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", Integer.parseInt(id));

		if (daoTeam.exist(TeamEntity.class, map, "and")) {
			team = daoTeam.buscaId(TeamEntity.class, Integer.parseInt(id));

			profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");
			if (team.getOwner().getId().equals(profile.getId())) {

				List<TeamProfileEntity> lista = new ArrayList<TeamProfileEntity>();
				List<TeamProfileEntity> list = new ArrayList<TeamProfileEntity>();

				Map<String, Object> mapTeamProfile = new HashMap<String, Object>();
				mapTeamProfile.put("team.id", Integer.parseInt(id));

				lista = daoTeamProfile.listarProperty(TeamProfileEntity.class, mapTeamProfile, "");
				for (TeamProfileEntity e : lista) {
					if (!e.getProfile().getId().equals(profile.getId())) {
						list.add(e);
					}
				}
				model.setViewName("team/createAdministrator");
				model.addObject("lista", list);
				model.addObject("team", team);

				return model;

			} else {

				model.setViewName("redirect:/team/viewTeam/" + id);
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/addAsAdmin/{idProfile}/{idTeam}", method = RequestMethod.GET)
	public ModelAndView addAsAdmin(@PathVariable(value = "idTeam") String idTeam,
			@PathVariable(value = "idProfile") String idProfile, ModelAndView model, HttpServletRequest request) {

		Map<String, Object> mapTeam = new HashMap<String, Object>();
		mapTeam.put("id", Integer.parseInt(idTeam));
		if (daoTeam.exist(TeamEntity.class, mapTeam, "and")) {

			team = daoTeam.buscaId(TeamEntity.class, Integer.parseInt(idTeam));

			profile2 = daoProfile.buscaId(ProfileEntity.class, Integer.parseInt(idProfile));
			profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

			if (team.getOwner().getId().equals(profile.getId())) {

				adminEntity.setAdmin(profile2);
				adminEntity.setTeam(team);

				daoTeamAdmin.saveUpdate(adminEntity);

				model.setViewName("redirect:/team/administrator/" + idTeam);

				return model;

			} else {

				model.setViewName("redirect:/team/viewTeam/" + idTeam);
				return model;
			}
		} else {
			model.setViewName("index/index");
			return model;
		}

	}

	@RequestMapping(value = "/viewUsers/{idTeam}", method = RequestMethod.GET)
	public ModelAndView viewUsers(ModelAndView model, HttpServletRequest request,
			@PathVariable(value = "idTeam") String idTeam) {

		Map<String, Object> mapTeamProfile = new HashMap<String, Object>();
		mapTeamProfile.put("team.id", Integer.parseInt(idTeam));

		listTeamProfile = daoTeamProfile.listarProperty(TeamProfileEntity.class, mapTeamProfile, "and");

		model.setViewName("team/viewUsers");
		model.addObject("lista", listTeamProfile);
		return model;
	}

}
