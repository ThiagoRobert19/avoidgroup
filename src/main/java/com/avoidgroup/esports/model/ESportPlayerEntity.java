package com.avoidgroup.esports.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


import org.springframework.stereotype.Component;

import com.avoidgroup.util.EntidadeBase;

@Component("ESportPlayerEntity")
@Entity
public class ESportPlayerEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String identificador;
	private String first_name;
	private String last_name;
	private String hometown;
	private String image_url;
	private String role;
	private String name;
	
	private String current_team;
	private String current_videogame;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getHometown() {
		return hometown;
	}
	public void setHometown(String hometown) {
		this.hometown = hometown;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCurrent_team() {
		return current_team;
	}
	public void setCurrent_team(String current_team) {
		this.current_team = current_team;
	}
	public String getCurrent_videogame() {
		return current_videogame;
	}
	public void setCurrent_videogame(String current_videogame) {
		this.current_videogame = current_videogame;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "ESportPlayerEntity [id=" + id + ", identificador=" + identificador + ", first_name=" + first_name
				+ ", last_name=" + last_name + ", hometown=" + hometown + ", image_url=" + image_url + ", role=" + role
				+ ", name=" + name + ", current_team=" + current_team + ", current_videogame=" + current_videogame
				+ "]";
	}
	
	
	
}
