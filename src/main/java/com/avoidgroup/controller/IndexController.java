package com.avoidgroup.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.servlet.ModelAndView;

import com.avoidgroup.dao.GenericDao;

import com.avoidgroup.model.FriendEntity;
import com.avoidgroup.model.GeneralCommentEntity;
import com.avoidgroup.model.GeneralPublicationEntity;
import com.avoidgroup.model.ProfileEntity;
import com.avoidgroup.model.UserEntity;
import com.avoidgroup.team.model.TeamEntity;
import com.avoidgroup.team.model.TeamProfileEntity;
import com.avoidgroup.twitchapi.model.TopGamesTwitchEntity;
import com.avoidgroup.twitchapi.util.TwitchApiUtil;
import com.avoidgroup.util.Common;
import com.avoidgroup.util.PublicationUtil;

@Controller
public class IndexController {
	@Autowired
	private GenericDao<GeneralPublicationEntity> daoPublication;

	@Autowired
	private Common common;

	@Autowired
	private UserEntity userEntity;

	@Autowired
	private ProfileEntity profile;

	@Autowired
	private GenericDao<FriendEntity> daoFriend;

	@Autowired
	private GenericDao<TeamProfileEntity> daoTeamProfile;

	@Autowired
	private GenericDao<ProfileEntity> daoProfile;

	@Autowired
	private GenericDao<TeamEntity> daoTeam;

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public ModelAndView home(HttpSession session, HttpServletRequest request, ModelAndView model,
			ArrayList<GeneralCommentEntity> listComments) throws IOException, ParseException {

		if (common.checkOnline(request)) {
			userEntity = (UserEntity) request.getSession().getAttribute("clienteLogado");

			Map<String, Object> mapUser = new HashMap<String, Object>();
			mapUser.put("user.id", userEntity.getId());

			if (!daoProfile.exist(ProfileEntity.class, mapUser, "")) {
				model.setViewName("redirect:/profile/register");
				return model;
			}

			profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

			Integer count = 0;
			count = daoFriend.count("FriendEntity", "profile1.id", profile.getId().toString())
					+ daoFriend.count("FriendEntity", "profile2.id", profile.getId().toString());
			PublicationUtil pubUtil = new PublicationUtil();
			List<GeneralPublicationEntity> listaPublication = new ArrayList<GeneralPublicationEntity>();

			if (count > 0) {
				listaPublication = pubUtil.getPublication(profile);
			} else {
				Integer contador = 0;
				contador = daoPublication.count("GeneralPublicationEntity", "publisher.id", profile.getId().toString());
				if (contador > 0) {
					listaPublication = pubUtil.getPublicationNoFriend(profile.getId());
				}

			}
			Integer qtTeam = daoTeamProfile.count("TeamProfileEntity", "profile.id", profile.getId().toString());

			TwitchApiUtil tw = new TwitchApiUtil();

			List<TopGamesTwitchEntity> listTopGames = new ArrayList<TopGamesTwitchEntity>();
			listTopGames = tw.getTopGames();

			model.addObject("listTopGames", listTopGames);

			model.addObject("qtTeam", qtTeam);
			model.addObject("friends", count);
			model.addObject("lista", listaPublication);
			model.addObject("profileLoged", profile);
			model.setViewName("index/index");

			return model;

		} else {

			model.setViewName("redirect:/user/login");

			return model;
		}

	}

	@RequestMapping(value = { "/chat" }, method = RequestMethod.GET)
	public ModelAndView chat(HttpSession session, HttpServletRequest request, ModelAndView model)
			throws IOException, ParseException {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");
		model.addObject("username", profile.getFirstName());
		model.setViewName("index/chat");

		return model;

	}

	@RequestMapping(value = "/search/general", method = RequestMethod.POST)
	public ModelAndView find(String name, HttpServletRequest request, ModelAndView model) {

		List<ProfileEntity> listaProfile = new ArrayList<ProfileEntity>();

		listaProfile = daoProfile.listBy3PropertyLike(ProfileEntity.class, "ProfileEntity", "firstName", "lastName",
				"nickName", name);

		List<TeamEntity> listaTeam = new ArrayList<TeamEntity>();

		listaTeam = daoTeam.listByPropertyLike(TeamEntity.class, "TeamEntity", "name", name);

		model.setViewName("index/search");
		model.addObject("listaFriend", listaProfile);
		model.addObject("listaTeam", listaTeam);

		return model;

	}

}