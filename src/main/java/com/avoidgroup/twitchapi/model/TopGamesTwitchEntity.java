package com.avoidgroup.twitchapi.model;




public class TopGamesTwitchEntity {
	

	private String game_id;
	private String game_img;
	private String game_name;
	private String game_link;
	public String getGame_id() {
		return game_id;
	}
	
	
	public String getGame_link() {
		return game_link;
	}


	public void setGame_link(String game_link) {
		this.game_link = game_link;
	}


	public void setGame_id(String game_id) {
		this.game_id = game_id;
	}
	public String getGame_img() {
		return game_img;
	}
	public void setGame_img(String game_img) {
		this.game_img = game_img;
	}
	public String getGame_name() {
		return game_name;
	}
	public void setGame_name(String game_name) {
		this.game_name = game_name;
	}


	@Override
	public String toString() {
		return "TopGamesTwitchEntity [game_id=" + game_id + ", game_img=" + game_img + ", game_name=" + game_name
				+ ", game_link=" + game_link + "]";
	}
	
	
	
	
}
