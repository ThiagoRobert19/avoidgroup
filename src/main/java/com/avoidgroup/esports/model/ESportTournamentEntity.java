package com.avoidgroup.esports.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.springframework.stereotype.Component;
import com.avoidgroup.util.EntidadeBase;

@Component("ESportTournamentEntity")
@Entity
public class ESportTournamentEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String identificador;
	private String begin_at;
	private String end_at;
	private String modified_at;
	private String name;
	private String slug;
	
	private String winner_id;
	private String winner_type;
	
	private String videogame;
	private String videogame_id;
	
	private String serie_id;
	private String serie_prize;
	private String serie_year;
	private String serie_begin;
	private String serie_season;
	private String serie_description;
	private String serie_end_at;
	private String serie_full_name;
	private String serie_name;
	
	
	private String league_id;
	private String league_name;
	
	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getBegin_at() {
		return begin_at;
	}
	public void setBegin_at(String begin_at) {
		this.begin_at = begin_at;
	}
	public String getEnd_at() {
		return end_at;
	}
	public void setEnd_at(String end_at) {
		this.end_at = end_at;
	}
	public String getModified_at() {
		return modified_at;
	}
	public void setModified_at(String modified_at) {
		this.modified_at = modified_at;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public String getSerie_id() {
		return serie_id;
	}
	public void setSerie_id(String serie_id) {
		this.serie_id = serie_id;
	}
	public String getSerie_prize() {
		return serie_prize;
	}
	public void setSerie_prize(String serie_prize) {
		this.serie_prize = serie_prize;
	}
	public String getSerie_year() {
		return serie_year;
	}
	public void setSerie_year(String serie_year) {
		this.serie_year = serie_year;
	}
	public String getSerie_begin() {
		return serie_begin;
	}
	public void setSerie_begin(String serie_begin) {
		this.serie_begin = serie_begin;
	}
	public String getSerie_season() {
		return serie_season;
	}
	public void setSerie_season(String serie_season) {
		this.serie_season = serie_season;
	}
	public String getSerie_description() {
		return serie_description;
	}
	public void setSerie_description(String serie_description) {
		this.serie_description = serie_description;
	}
	public String getSerie_end_at() {
		return serie_end_at;
	}
	public void setSerie_end_at(String serie_end_at) {
		this.serie_end_at = serie_end_at;
	}
	public String getSerie_full_name() {
		return serie_full_name;
	}
	public void setSerie_full_name(String serie_full_name) {
		this.serie_full_name = serie_full_name;
	}
	public String getSerie_name() {
		return serie_name;
	}
	public void setSerie_name(String serie_name) {
		this.serie_name = serie_name;
	}
	public String getLeague_id() {
		return league_id;
	}
	public void setLeague_id(String league_id) {
		this.league_id = league_id;
	}
	public String getLeague_name() {
		return league_name;
	}
	public void setLeague_name(String league_name) {
		this.league_name = league_name;
	}
	public String getVideogame() {
		return videogame;
	}
	public void setVideogame(String videogame) {
		this.videogame = videogame;
	}
	public String getVideogame_id() {
		return videogame_id;
	}
	public void setVideogame_id(String videogame_id) {
		this.videogame_id = videogame_id;
	}
	public String getWinner_id() {
		return winner_id;
	}
	public void setWinner_id(String winner_id) {
		this.winner_id = winner_id;
	}
	public String getWinner_type() {
		return winner_type;
	}
	public void setWinner_type(String winner_type) {
		this.winner_type = winner_type;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "ESportTournamentEntity [id=" + id + ", identificador=" + identificador + ", begin_at=" + begin_at
				+ ", end_at=" + end_at + ", modified_at=" + modified_at + ", name=" + name + ", slug=" + slug
				+ ", serie_id=" + serie_id + ", serie_prize=" + serie_prize + ", serie_year=" + serie_year
				+ ", serie_begin=" + serie_begin + ", serie_season=" + serie_season + ", serie_description="
				+ serie_description + ", serie_end_at=" + serie_end_at + ", serie_full_name=" + serie_full_name
				+ ", serie_name=" + serie_name + ", league_id=" + league_id + ", league_name=" + league_name
				+ ", videogame=" + videogame + ", videogame_id=" + videogame_id + ", winner_id=" + winner_id
				+ ", winner_type=" + winner_type + "]";
	}
	
	
	
}
