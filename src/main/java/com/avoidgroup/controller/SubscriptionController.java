package com.avoidgroup.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.avoidgroup.model.ProfileEntity;

@Controller
@RequestMapping(value = "/subscription")
public class SubscriptionController {
	
	@Autowired
	private ProfileEntity profile;

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view(HttpServletRequest request, HttpServletResponse response, ModelAndView model) {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");
		model.setViewName("subscription/view");
		return model;
	}
}
