package com.avoidgroup.twitch.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.avoidgroup.dao.GenericDao;
import com.avoidgroup.model.ProfileEntity;
import com.avoidgroup.twitchapi.model.ProfileTwitchEntity;
import com.avoidgroup.twitchapi.util.TwitchApiUtil;

@Controller
@RequestMapping(value = "/twitch")
public class TwitchApiController {
	@Autowired
	private ProfileTwitchEntity profileTwitch;
	@Autowired
	private GenericDao<ProfileTwitchEntity> daoTwitch;

	@Autowired
	private ProfileEntity profile;
	
	@RequestMapping(value = { "/auth" }, method = RequestMethod.GET)
	public ModelAndView verify(HttpServletRequest request, HttpServletResponse response, ModelAndView model)
			throws IOException {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		String code = request.getParameter("code");
		TwitchApiUtil twitchapi = new TwitchApiUtil();

		String acces_token = twitchapi.getAccessToken(code);
		profileTwitch = twitchapi.getUser(acces_token);
		profileTwitch.setProfile(profile);

		System.out.println(profileTwitch.getTwitchID());
		daoTwitch.saveUpdate(profileTwitch);
		twitchapi.revokeAccessToken(acces_token);

		model.setViewName("redirect:/profile/view");
		return model;
	}

	@RequestMapping(value = { "/logout" }, method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest request, ModelAndView model) {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("profile.id", profile.getId());

		daoTwitch.delete(ProfileTwitchEntity.class, map, "and");

		model.setViewName("redirect:/profile/view");

		return model;
	}
}
