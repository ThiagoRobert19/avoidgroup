package com.avoidgroup.esports.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.stereotype.Component;

import com.avoidgroup.util.EntidadeBase;

@Component("ESportSerieEntity")
@Entity
public class ESportSerieEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String identificador;
	private String begin_at;
	private String description;
	private String end_at;
	private String full_name;
	private String modified_at;
	private String name;
	private String prizepool;
	private String season;
	private String slug;
	private String winner_id;
	private String winner_type;
	private String year;

	private String league_id;
	private String league_name;

	private String videogame;
	private String videogame_id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getBegin_at() {
		return begin_at;
	}

	public void setBegin_at(String begin_at) {
		this.begin_at = begin_at;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEnd_at() {
		return end_at;
	}

	public void setEnd_at(String end_at) {
		this.end_at = end_at;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getModified_at() {
		return modified_at;
	}

	public void setModified_at(String modified_at) {
		this.modified_at = modified_at;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrizepool() {
		return prizepool;
	}

	public void setPrizepool(String prizepool) {
		this.prizepool = prizepool;
	}

	public String getSeason() {
		return season;
	}

	public void setSeason(String season) {
		this.season = season;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getWinner_id() {
		return winner_id;
	}

	public void setWinner_id(String winner_id) {
		this.winner_id = winner_id;
	}

	public String getWinner_type() {
		return winner_type;
	}

	public void setWinner_type(String winner_type) {
		this.winner_type = winner_type;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getLeague_id() {
		return league_id;
	}

	public void setLeague_id(String league_id) {
		this.league_id = league_id;
	}

	public String getLeague_name() {
		return league_name;
	}

	public void setLeague_name(String league_name) {
		this.league_name = league_name;
	}

	public String getVideogame() {
		return videogame;
	}

	public void setVideogame(String videogame) {
		this.videogame = videogame;
	}

	public String getVideogame_id() {
		return videogame_id;
	}

	public void setVideogame_id(String videogame_id) {
		this.videogame_id = videogame_id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ESportSerieEntity [id=" + id + ", identificador=" + identificador + ", begin_at=" + begin_at
				+ ", description=" + description + ", end_at=" + end_at + ", full_name=" + full_name + ", modified_at="
				+ modified_at + ", name=" + name + ", prizepool=" + prizepool + ", season=" + season + ", slug=" + slug
				+ ", winner_id=" + winner_id + ", winner_type=" + winner_type + ", year=" + year + ", league_id="
				+ league_id + ", league_name=" + league_name + ", videogame=" + videogame + ", videogame_id="
				+ videogame_id + "]";
	}

}
