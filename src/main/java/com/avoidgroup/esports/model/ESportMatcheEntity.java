package com.avoidgroup.esports.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import org.springframework.stereotype.Component;
import com.avoidgroup.util.EntidadeBase;

@Component("ESportMatcheEntity")
@Entity
public class ESportMatcheEntity   implements Serializable, EntidadeBase{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String begin_at;
	private String match_type;
	private String modified_at;
	private String name;
	private String slug;
	private String status;
	
	private String score;
	
	@OneToOne
	@JoinColumn(name = "tournament_id")
	private ESportTournamentEntity tournament;
	
	@ManyToMany(fetch = FetchType.EAGER)
	private List<ESportPlayerEntity> player;
	
	@ManyToMany(fetch = FetchType.EAGER)
	private List<ESportTeamEntity> team;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBegin_at() {
		return begin_at;
	}

	public void setBegin_at(String begin_at) {
		this.begin_at = begin_at;
	}

	public String getMatch_type() {
		return match_type;
	}

	public void setMatch_type(String match_type) {
		this.match_type = match_type;
	}

	public String getModified_at() {
		return modified_at;
	}

	public void setModified_at(String modified_at) {
		this.modified_at = modified_at;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public ESportTournamentEntity getTournament() {
		return tournament;
	}

	public void setTournament(ESportTournamentEntity tournament) {
		this.tournament = tournament;
	}

	public List<ESportPlayerEntity> getPlayer() {
		return player;
	}

	public void setPlayer(List<ESportPlayerEntity> player) {
		this.player = player;
	}

	public List<ESportTeamEntity> getTeam() {
		return team;
	}

	public void setTeam(List<ESportTeamEntity> team) {
		this.team = team;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ESportMatcheEntity [id=" + id + ", begin_at=" + begin_at + ", match_type=" + match_type
				+ ", modified_at=" + modified_at + ", name=" + name + ", slug=" + slug + ", status=" + status
				+ ", score=" + score + ", tournament=" + tournament + ", player=" + player + ", team=" + team + "]";
	}
	
	




}
