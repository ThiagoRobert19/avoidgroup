package com.avoidgroup.esports.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PandaScoreMatchesUtil {

	private static final String ACCESS_TOKEN = "sL3PReaZ8uJPnbKFbMrIEukZ7rOxpaEQ-xmPRwNkHl_sBfB9cLc";

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	private static JSONArray readJsonFromUrl(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);

			JSONArray values = new JSONArray(jsonText);
			return values;
		} finally {
			is.close();
		}
	}

	public JSONArray GetGameSearchMatches(Integer page, String game, String dateRange1, String dateRange2) {
		System.out.println("carregando " + game);
		try {
			JSONArray values = readJsonFromUrl("https://api.pandascore.co/" + game + "/matches?range[begin_at]="
					+ dateRange1 + "," + dateRange2 + "&page=" + page + "&per_page=100&?&token=" + ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			System.out.println("erro 2: " + e.getMessage().toString());
			return null;
		}

	}

	public JSONArray GetGamesMatches(Integer page, String umpassado, String umfuturo) {
		try {

			JSONArray values = readJsonFromUrl("https://api.pandascore.co/matches?range[begin_at]=" + umpassado + ","
					+ umfuturo + "&page=" + page + "&per_page=100&?&token=" + ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			System.out.println("erro 2: " + e.getMessage().toString());
			return null;
		}

	}

	public JSONArray GetTournaments(Integer page) {
		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/tournaments?page=" + page + "&per_page=100&?&token=" + ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			System.out.println("erro 2: " + e.getMessage().toString());
			return null;
		}

	}

	public JSONArray GetPlayers(Integer page) {
		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/players?page=" + page + "&per_page=100&?&token=" + ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			System.out.println("erro 2: " + e.getMessage().toString());
			return null;
		}

	}

	public JSONArray GetTeams(Integer page) {
		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/teams?page=" + page + "&per_page=100&?&token=" + ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			System.out.println("erro 2: " + e.getMessage().toString());
			return null;
		}

	}

	public JSONArray GetLeagues(Integer page) {
		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/leagues?page=" + page + "&per_page=100&?&token=" + ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			System.out.println("erro 2: " + e.getMessage().toString());
			return null;
		}

	}

	public JSONArray GetSeries(Integer page, String anopassado, String anofuturo) {
		try {

			JSONArray values = readJsonFromUrl("https://api.pandascore.co/series?range[year]=" + anopassado + ","
					+ anofuturo + "&page=" + page + "&per_page=100&?&token=" + ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			System.out.println("erro 2: " + e.getMessage().toString());
			return null;
		}

	}

}
