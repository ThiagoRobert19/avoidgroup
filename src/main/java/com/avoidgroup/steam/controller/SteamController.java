package com.avoidgroup.steam.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.expressme.openid.Association;
import org.expressme.openid.Endpoint;
import org.expressme.openid.OpenIdException;
import org.expressme.openid.OpenIdManager;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.avoidgroup.dao.GenericDao;
import com.avoidgroup.model.ProfileEntity;
import com.avoidgroup.steam.model.GameListSteam;
import com.avoidgroup.steam.model.NewsForAppEntity;
import com.avoidgroup.steam.model.ProfileSteamEntity;
import com.avoidgroup.steam.model.RecentlyPlayedEntity;
import com.avoidgroup.steam.model.SummarieEntity;
import com.avoidgroup.util.InfoSteam;

@Controller
@RequestMapping(value = "/steam")
public class SteamController {

	@Autowired
	private ProfileEntity profile;

	@Autowired
	private ProfileSteamEntity profileSteam;

	@Autowired
	private GenericDao<ProfileEntity> daoProfile;

	@Autowired
	private GenericDao<ProfileSteamEntity> daoSteam;

	static final long ONE_HOUR = 3600000L;
	static final long TWO_HOUR = ONE_HOUR * 2L;
	static final String ATTR_MAC = "openid_mac";
	static final String ATTR_ALIAS = "openid_alias";

	@RequestMapping(value = "/newsapp/{appid}/{name}/{image_url}", method = RequestMethod.GET)
	public ModelAndView newsapp(@PathVariable(value = "appid") String appid, @PathVariable(value = "name") String name,
			@PathVariable(value = "image_url") String image_url, ModelAndView model, HttpServletRequest request) {

		List<NewsForAppEntity> newsList = new ArrayList<NewsForAppEntity>();

		JSONArray apps = new InfoSteam().GetNewsApp(appid);
		if (apps != null) {

			for (int i = 0; i < apps.length(); i++) {

				JSONObject value = apps.getJSONObject(i);
				NewsForAppEntity newsApp = new NewsForAppEntity();

				newsApp.setImage_url(image_url);
				newsApp.setGame_name(name);
				newsApp.setGame_appid(appid);

				newsApp.setContents(value.get("contents").toString());
				newsApp.setFeedlabel(value.get("feedlabel").toString());
				// newsApp.setIs_external_url(value.get("is_external_url").toString());
				newsApp.setTitle(value.get("title").toString());
				newsApp.setUrl(value.get("url").toString());

				String date_new = value.get("date").toString();

				long unixSeconds = Long.parseLong(date_new);

				Date date = new java.util.Date(unixSeconds * 1000L);

				SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss ");
				date_new = sdf.format(date);

				newsApp.setDate_new(date_new);
				newsList.add(newsApp);
			}

		}
		model.addObject("newsList", newsList);
		model.addObject("name", name);
		model.addObject("image_url", image_url);
		model.addObject("appid", appid);
		Integer idProfile = (Integer) request.getSession().getAttribute("idProfile");

		profile = daoProfile.buscaId(ProfileEntity.class, idProfile);
		model.addObject("profileLoged", profile);

		model.setViewName("steam/newsapp");

		return model;

	}

	@RequestMapping(value = "/frienddata/{id}", method = RequestMethod.GET)
	public ModelAndView frienddata(@PathVariable(value = "id") String id, ModelAndView model,
			HttpServletRequest request) {

		profile = daoProfile.buscaId(ProfileEntity.class, Integer.parseInt(id));

		Map<String, Object> mapsteam = new HashMap<String, Object>();
		mapsteam.put("profile.id", profile.getId());

		profileSteam = daoSteam.findByProperty(ProfileSteamEntity.class, mapsteam, "and");

		String SteamId = profileSteam.getSteamID();

		List<RecentlyPlayedEntity> listPlayed = new ArrayList<RecentlyPlayedEntity>();

		JSONArray values = new InfoSteam().GetRecentlyPlayedGames(SteamId);
		System.out.println("teste steam");
		if (values != null) {

			for (int i = 0; i < values.length(); i++) {

				RecentlyPlayedEntity rp = new RecentlyPlayedEntity();

				JSONObject value = values.getJSONObject(i);

				rp.setAppid((Integer) value.get("appid"));
				rp.setName(value.getString("name"));
				rp.setPlaytime_2weeks(value.get("playtime_2weeks").toString());
				rp.setPlaytime_forever(value.get("playtime_forever").toString());
				rp.setProfile(profile);
				rp.setImageURL(value.getString("img_logo_url"));

				listPlayed.add(rp);
			}
		}
		JSONArray valueSummaries = new InfoSteam().GetSummarie(SteamId);

		SummarieEntity se = new SummarieEntity();
		if (valueSummaries != null) {

			for (int i = 0; i < valueSummaries.length(); i++) {

				JSONObject value = valueSummaries.getJSONObject(i);

				String avatar = value.get("avatarfull").toString();
				String persona = value.get("personaname").toString();
				String lastLogoff = value.get("lastlogoff").toString();
				String profileurl = value.get("profileurl").toString();
				String timecreated = value.get("timecreated").toString();

				long unixSeconds = Long.parseLong(lastLogoff);

				Date date = new java.util.Date(unixSeconds * 1000L);

				SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss ");
				lastLogoff = sdf.format(date);

				unixSeconds = Long.parseLong(timecreated);

				date = new java.util.Date(unixSeconds * 1000L);
				timecreated = sdf.format(date);
				se.setTimecreated(timecreated);
				se.setAvatarmedium(avatar);
				se.setLastlogoff(lastLogoff);
				se.setPersonaname(persona);
				se.setProfileurl(profileurl);
				JSONObject v = new InfoSteam().GetSteamLevel(SteamId);
				String level = v.get("player_level").toString();
				se.setLevel(level);

			}
		}

		List<GameListSteam> gameList = new ArrayList<GameListSteam>();

		JSONArray games = new InfoSteam().GetOwnedGames(SteamId);
		if (games != null) {
			System.out.println("value nao eh null: " + games.length());
			for (int i = 0; i < games.length(); i++) {

				GameListSteam gl = new GameListSteam();

				JSONObject value = games.getJSONObject(i);
				gl.setAppid((Integer) value.get("appid"));
				gl.setImg_logo_url(value.getString("img_logo_url"));

				gl.setName(value.getString("name"));

				gl.setPlaytime_forever(value.get("playtime_forever").toString());

				long unixSeconds = Long.parseLong(gl.getPlaytime_forever());

				Date date = new java.util.Date(unixSeconds * 1000L);

				SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm:ss ");

				gl.setPlaytime_forever(sdf.format(date));

				gameList.add(gl);
			}

		}

		model.addObject("gameList", gameList);

		model.setViewName("steam/frienddata");
		model.addObject("profile", profile);
		model.addObject("profileLoged", profile);
		model.addObject("summaries", se);
		model.addObject("listPlayed", listPlayed);
		return model;

	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(ModelAndView model, HttpServletRequest request) {

		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapsteam = new HashMap<String, Object>();
		mapsteam.put("profile.id", profile.getId());

		daoSteam.delete(ProfileSteamEntity.class, mapsteam, "and");

		model.setViewName("redirect:/profile/view");

		return model;

	}

	@RequestMapping(value = "/yourdata", method = RequestMethod.GET)
	public ModelAndView data(ModelAndView model, HttpServletRequest request) {

		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapsteam = new HashMap<String, Object>();
		mapsteam.put("profile.id", profile.getId());

		profileSteam = daoSteam.findByProperty(ProfileSteamEntity.class, mapsteam, "and");

		String SteamId = profileSteam.getSteamID();

		List<RecentlyPlayedEntity> listPlayed = new ArrayList<RecentlyPlayedEntity>();

		// JSONArray values = new InfoSteam().GetRecentlyPlayedGames(SteamId);
		JSONArray values = new InfoSteam().GetRecentlyPlayedGames(SteamId);
		if (values != null) {
			System.out.println("value nao eh null: " + values.length());
			for (int i = 0; i < values.length(); i++) {

				RecentlyPlayedEntity rp = new RecentlyPlayedEntity();

				JSONObject value = values.getJSONObject(i);

				rp.setAppid((Integer) value.get("appid"));
				rp.setName(value.getString("name"));
				rp.setPlaytime_2weeks(value.get("playtime_2weeks").toString());
				rp.setPlaytime_forever(value.get("playtime_forever").toString());
				rp.setProfile(profile);
				rp.setImageURL(value.getString("img_logo_url"));

				listPlayed.add(rp);
			}
		}

		// JSONArray valueSummaries = new InfoSteam().GetSummarie(SteamId);
		JSONArray valueSummaries = new InfoSteam().GetSummarie(SteamId);
		SummarieEntity se = new SummarieEntity();
		if (valueSummaries != null) {
			System.out.println("value nao eh null: " + valueSummaries.length());
			for (int i = 0; i < valueSummaries.length(); i++) {

				JSONObject value = valueSummaries.getJSONObject(i);

				String avatar = value.get("avatarfull").toString();
				String persona = value.get("personaname").toString();
				String lastLogoff = value.get("lastlogoff").toString();
				String profileurl = value.get("profileurl").toString();
				String timecreated = value.get("timecreated").toString();

				long unixSeconds = Long.parseLong(lastLogoff);

				Date date = new java.util.Date(unixSeconds * 1000L);

				SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss ");
				lastLogoff = sdf.format(date);

				unixSeconds = Long.parseLong(timecreated);

				date = new java.util.Date(unixSeconds * 1000L);
				timecreated = sdf.format(date);
				se.setTimecreated(timecreated);
				se.setAvatarmedium(avatar);
				se.setLastlogoff(lastLogoff);
				se.setPersonaname(persona);
				se.setProfileurl(profileurl);

				JSONObject v = new InfoSteam().GetSteamLevel(SteamId);
				String level = "";
				if (v.get("player_level").toString() == null || v.get("player_level").toString().equals("")) {
					level = "unknow";
				} else {
					level = v.get("player_level").toString();
				}

				se.setLevel(level);

			}
		}

		List<GameListSteam> gameList = new ArrayList<GameListSteam>();

		// JSONArray games = new InfoSteam().GetOwnedGames(SteamId);
		JSONArray games = new InfoSteam().GetOwnedGames(SteamId);
		if (games != null) {
			System.out.println("value nao eh null: " + games.length());
			for (int i = 0; i < games.length(); i++) {

				GameListSteam gl = new GameListSteam();

				JSONObject value = games.getJSONObject(i);
				gl.setAppid((Integer) value.get("appid"));
				gl.setImg_logo_url(value.getString("img_logo_url"));

				gl.setName(value.getString("name"));

				gl.setPlaytime_forever(value.get("playtime_forever").toString());

				long unixSeconds = Long.parseLong(gl.getPlaytime_forever());

				Date date = new java.util.Date(unixSeconds * 1000L);

				SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm:ss ");

				gl.setPlaytime_forever(sdf.format(date));

				gameList.add(gl);
			}

		}

		model.addObject("gameList", gameList);
		model.addObject("profileLoged", profile);
		model.setViewName("steam/yourdata");
		model.addObject("summaries", se);
		model.addObject("listPlayed", listPlayed);
		return model;

	}

	@RequestMapping(value = { "/login/simple/{steam}" }, method = RequestMethod.GET)
	protected void doGet(@PathVariable(value = "steam") String steam, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// PrintWriter out = response.getWriter();
		OpenIdManager manager = new OpenIdManager();
		String path = request.getRequestURL().toString();

		String managerReal = "";
		String managerReturnTo = "";
		String responseRedirect = "";

		if (path.contains("localhost")) {
			managerReal = "http://localhost:8080/AvoidGroup/steam/login";
			managerReturnTo = "http://localhost:8080/AvoidGroup/steam/login/simple";
			responseRedirect = "http://localhost:8080/AvoidGroup/profile/view";
		} else {
			managerReal = "https://www.avoidgroup.com/steam/login";
			managerReturnTo = "https://www.avoidgroup.com/steam/login/simple";
			responseRedirect = "https://www.avoidgroup.com/profile/view";

		}
		manager.setRealm(managerReal);
		manager.setReturnTo(managerReturnTo);

		String login = steam;
		if (login != null) {
			if (login.equals("steam")) {

				Endpoint endpoint = manager.lookupEndpoint("https://steamcommunity.com/openid/");
				Association association = manager.lookupAssociation(endpoint);
				request.getSession().setAttribute(ATTR_MAC, association.getRawMacKey());
				request.getSession().setAttribute(ATTR_ALIAS, endpoint.getAlias());
				String url = manager.getAuthenticationUrl(endpoint, association);
				System.out.println("url: " + url);
				response.sendRedirect(url);
			} else if (login.equals("verify")) {
				checkNonce(request.getParameter("openid.response_nonce"));
				// byte[] mac_key = (byte[])
				// request.getSession().getAttribute(ATTR_MAC);
				// String alias = (String)
				// request.getSession().getAttribute(ATTR_ALIAS);

				response.setContentType("text/html; charset=UTF-8");

				String identity = request.getParameter("openid.identity");
				String[] array = identity.split("/id/");
				request.getSession().setAttribute("steamid", array[1]);
				response.sendRedirect(responseRedirect);

			} else if (login.equals("logout")) {
				request.getSession().removeAttribute("steamid");
				response.sendRedirect(responseRedirect);

			}
		}
	}

	@RequestMapping(value = { "/login/simple" }, method = RequestMethod.GET)
	void verify(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String path = request.getRequestURL().toString();
		String responseRedirect = "";

		if (path.contains("localhost")) {

			responseRedirect = "http://localhost:8080/AvoidGroup/profile/view";
		} else {

			responseRedirect = "https://www.avoidgroup.com/profile/view";

		}

		// ================

		String login = "verify";
		if (login.equals("verify")) {
			checkNonce(request.getParameter("openid.response_nonce"));

			response.setContentType("text/html; charset=UTF-8");

			String identity = request.getParameter("openid.identity");
			String[] array = identity.split("/id/");
			request.getSession().setAttribute("steamid", array[1]);
			response.sendRedirect(responseRedirect);

			String ii = (String) request.getSession().getAttribute("steamid");
			Integer idProfile = (Integer) request.getSession().getAttribute("idProfile");

			profile = daoProfile.buscaId(ProfileEntity.class, idProfile);

			profileSteam.setProfile(profile);
			profileSteam.setSteamID(ii);
			daoSteam.saveUpdate(profileSteam);

		}
	}

	void checkNonce(String nonce) {
		// check response_nonce to prevent replay-attack:
		if (nonce == null || nonce.length() < 20)
			throw new OpenIdException("Verify failed.");
		// make sure the time of server is correct:
		long nonceTime = getNonceTime(nonce);
		long diff = Math.abs(System.currentTimeMillis() - nonceTime);
		if (diff > ONE_HOUR)
			throw new OpenIdException("Bad nonce time.");
		if (isNonceExist(nonce))
			throw new OpenIdException("Verify nonce failed.");
		storeNonce(nonce, nonceTime + TWO_HOUR);
	}

	private Set<String> nonceDb = new HashSet<String>();

	// check if nonce is exist in database:
	boolean isNonceExist(String nonce) {
		return nonceDb.contains(nonce);
	}

	// store nonce in database:
	void storeNonce(String nonce, long expires) {
		nonceDb.add(nonce);
	}

	long getNonceTime(String nonce) {
		try {
			return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(nonce.substring(0, 19) + "+0000").getTime();
		} catch (ParseException e) {
			throw new OpenIdException("Bad nonce time.");
		}
	}
}