package com.avoidgroup.stripe.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.avoidgroup.esports.model.MatchOfTheDayEntity;
import com.avoidgroup.twitchapi.model.ProfileTwitchEntity;
import com.avoidgroup.twitchapi.model.TopGamesTwitchEntity;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Balance;
import com.stripe.model.BalanceTransaction;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import com.stripe.model.Plan;
import com.stripe.model.Product;
import com.stripe.model.Subscription;
import com.stripe.model.SubscriptionCollection;

public class StripeAPIUtil {
	private static String PUBLISHABLE_KEY = "pk_test_J2aeYcsXf3VA0A0wlWmewRqA00kdSsqBAP";
	private static String SECRET_KEY = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

	// ################################################################################################
	public static void createACharge() throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

	//	String token = request.getParameter("stripeToken");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("amount", 500);
		params.put("currency", "usd");

		params.put("source", "tok_visa");
		params.put("receipt_email", "thiago_robert19@hotmail.com");
		Charge charge = Charge.create(params);
		System.out.println(charge);

	}

	public static void getACharge(String chargeID) throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		System.out.println(Charge.retrieve(chargeID));

	}

	public static void getAllCharge() throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		Map<String, Object> chargeParams = new HashMap<String, Object>();
		chargeParams.put("limit", "10");

		System.out.println(Charge.list(chargeParams));

	}

	// ################################################################################################
	public static void getBalance() throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		System.out.println(Balance.retrieve());

	}

	public static void getBalanceHistory() throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		Map<String, Object> balancetransactionParams = new HashMap<String, Object>();
		balancetransactionParams.put("limit", 3);

		System.out.println(BalanceTransaction.list(balancetransactionParams));
	}

	public static void getBalanceTransaction(String TransactionID) throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		System.out.println(BalanceTransaction.retrieve(TransactionID));
	}

	// ################################################################################################
	public static void createACustomer() throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		Map<String, Object> customerParams = new HashMap<String, Object>();
		customerParams.put("description", "Customer for jenny.rosen@example.com");
		customerParams.put("source", "tok_visa");
		// ^ obtained with Stripe.js
		Customer customer = Customer.create(customerParams);
		System.out.println(customer);

	}

	public static void getACustomer(String customerID) throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		System.out.println(Customer.retrieve(customerID));

	}

	public static void UpdateACustomer(String customerID) throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		Customer customer = Customer.retrieve(customerID);
		Map<String, Object> metadata = new HashMap<>();
		metadata.put("order_id", "6735");
		Map<String, Object> params = new HashMap<>();
		params.put("metadata", metadata);
		customer.update(params);
		System.out.println(customer);
	}

	public static void getAllCustomer() throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		Map<String, Object> customerParams = new HashMap<String, Object>();
		customerParams.put("limit", "3");
		System.out.println(Customer.list(customerParams));

	}

	// ################################################################################################
	public static void createAProduct() throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		Map<String, Object> productParams = new HashMap<String, Object>();
		productParams.put("name", "Monthly membership base fee");
		productParams.put("type", "service");

		Product product = Product.create(productParams);
		System.out.println(product);

	}

	public static void getAProduct(String productID) throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		Product product = Product.retrieve(productID);
		System.out.println(product);
	}

	public static void getAllProduct() throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		Map<String, Object> productParams = new HashMap<String, Object>();
		productParams.put("limit", "10");

		System.out.println(Product.list(productParams));

	}

	public static void updateAProduct() throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		Product product = Product.retrieve("prod_EuSfog85osEAVb");
		Map<String, Object> metadata = new HashMap<>();
		metadata.put("order_id", "6735");
		Map<String, Object> params = new HashMap<>();
		params.put("metadata", metadata);
		product.update(params);

		System.out.println(product);

	}

	public static void deleteAProduct(String subscriptionID) throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		Product product = Product.retrieve("prod_EuSfog85osEAVb");
		product.delete();

	}

	// ################################################################################################
	public static void createAPlan() throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		Map<String, Object> productParams = new HashMap<String, Object>();
		productParams.put("name", "Gold special");

		Map<String, Object> planParams = new HashMap<String, Object>();
		planParams.put("amount", 500);
		planParams.put("interval", "month");
		planParams.put("product", "productParams");
		planParams.put("currency", "usd");

		Plan plan = Plan.create(planParams);
		System.out.println(plan);

	}

	public static void getAPlan(String subscriptionID) throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		Plan plan = Plan.retrieve("gold");
		System.out.println(plan);
	}

	public static void getAllPlan() throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		Map<String, Object> planParams = new HashMap<String, Object>();
		planParams.put("limit", "3");

		System.out.println(Plan.list(planParams));

	}

	public static void updateAPlan() throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		Plan plan = Plan.retrieve("gold");
		Map<String, Object> metadata = new HashMap<>();
		metadata.put("order_id", "6735");
		Map<String, Object> params = new HashMap<>();
		params.put("metadata", metadata);
		plan.update(params);

		System.out.println(plan);

	}

	public static void deleteAPlan(String subscriptionID) throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		Plan plan = Plan.retrieve("gold");
		plan.delete();

	}

	// ################################################################################################
	public static void createASubscription() throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		Map<String, Object> item = new HashMap<String, Object>();
		item.put("plan", "gold");

		Map<String, Object> items = new HashMap<String, Object>();
		items.put("0", item);

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("customer", "cus_EuSEvqA19tjslp");
		params.put("items", items);

		Subscription subscription = Subscription.create(params);
		System.out.println(subscription);

	}

	public static void getASubscription(String subscriptionID) throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		System.out.println(Subscription.retrieve(subscriptionID));
	}

	public static void getAllSubscription() throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("limit", 3);
		SubscriptionCollection subscriptions = Subscription.list(params);

		System.out.println(subscriptions);

	}

	public static void cancelASubscription(String subscriptionID) throws StripeException {
		Stripe.apiKey = "sk_test_q1j9yoPuIrBE3zjoOwOGK57c00qvUkoOS3";

		Subscription sub = Subscription.retrieve(subscriptionID);
		sub.cancel(null);

	}

	public static void main(String[] args) throws Exception {

		// teste();
		// getBalance();

	}
}
