package com.avoidgroup.steam.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.avoidgroup.util.EntidadeBase;

@Entity
public class GameListSteam implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private int appid;
	private String name;
	private String playtime_forever;
	private String img_logo_url;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getAppid() {
		return appid;
	}
	public void setAppid(int appid) {
		this.appid = appid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPlaytime_forever() {
		return playtime_forever;
	}
	public void setPlaytime_forever(String playtime_forever) {
		this.playtime_forever = playtime_forever;
	}
	public String getImg_logo_url() {
		return img_logo_url;
	}
	public void setImg_logo_url(String img_logo_url) {
		this.img_logo_url = img_logo_url;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "GameListSteam [id=" + id + ", appid=" + appid + ", name=" + name + ", playtime_forever="
				+ playtime_forever + ", img_logo_url=" + img_logo_url + "]";
	}

	
}
