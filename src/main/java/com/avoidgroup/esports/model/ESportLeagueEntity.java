package com.avoidgroup.esports.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.springframework.stereotype.Component;

import com.avoidgroup.util.EntidadeBase;

@Component("ESportLeagueEntity")
@Entity
public class ESportLeagueEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String identificador;
	private String image_url;
	private String live_supported;// yes no
	private String modified_at;
	private String name;
	private String slug;

	private String url;

	private String videogame;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getLive_supported() {
		return live_supported;
	}

	public void setLive_supported(String live_supported) {
		this.live_supported = live_supported;
	}

	public String getModified_at() {
		return modified_at;
	}

	public void setModified_at(String modified_at) {
		this.modified_at = modified_at;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getVideogame() {
		return videogame;
	}

	public void setVideogame(String videogame) {
		this.videogame = videogame;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ESportLeagueEntity [id=" + id + ", identificador=" + identificador + ", image_url=" + image_url
				+ ", live_supported=" + live_supported + ", modified_at=" + modified_at + ", name=" + name + ", slug="
				+ slug + ", url=" + url + ", videogame=" + videogame + "]";
	}

}
