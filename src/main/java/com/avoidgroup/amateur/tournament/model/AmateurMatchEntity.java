package com.avoidgroup.amateur.tournament.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.avoidgroup.team.model.TeamEntity;
import com.avoidgroup.util.EntidadeBase;

@Component("AmateurMatchEntity")
@Entity
public class AmateurMatchEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Temporal(TemporalType.DATE)
	private Date begin_date;

	@DateTimeFormat(pattern = "HH:mm:ss")
	@Temporal(TemporalType.TIME)
	private Date begin_hour;

	@Temporal(TemporalType.DATE)
	private Date end_date;

	@OneToOne
	@JoinColumn(name = "tournament_id")
	private AmateurTournamentEntity tournament;

	@OneToOne
	@JoinColumn(name = "team1_id")
	private TeamEntity team1;

	@OneToOne
	@JoinColumn(name = "team2_id")
	private TeamEntity team2;

	private String result1;

	private String result2;

	private String status;
	private String match_type;

	@OneToOne
	@JoinColumn(name = "winner_id")
	private TeamEntity winner;

	private String match_name;

	public String getMatch_type() {
		return match_type;
	}

	public void setMatch_type(String match_type) {
		this.match_type = match_type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getBegin_date() {
		return begin_date;
	}

	public void setBegin_date(Date begin_date) {
		this.begin_date = begin_date;
	}

	public Date getBegin_hour() {
		return begin_hour;
	}

	public void setBegin_hour(Date begin_hour) {
		this.begin_hour = begin_hour;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public AmateurTournamentEntity getTournament() {
		return tournament;
	}

	public void setTournament(AmateurTournamentEntity tournament) {
		this.tournament = tournament;
	}

	public TeamEntity getTeam1() {
		return team1;
	}

	public void setTeam1(TeamEntity team1) {
		this.team1 = team1;
	}

	public TeamEntity getTeam2() {
		return team2;
	}

	public void setTeam2(TeamEntity team2) {
		this.team2 = team2;
	}

	public String getResult1() {
		return result1;
	}

	public void setResult1(String result1) {
		this.result1 = result1;
	}

	public String getResult2() {
		return result2;
	}

	public void setResult2(String result2) {
		this.result2 = result2;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public TeamEntity getWinner() {
		return winner;
	}

	public void setWinner(TeamEntity winner) {
		this.winner = winner;
	}

	public String getMatch_name() {
		return match_name;
	}

	public void setMatch_name(String match_name) {
		this.match_name = match_name;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "AmateurMatchEntity [id=" + id + ", begin_date=" + begin_date + ", begin_hour=" + begin_hour
				+ ", end_date=" + end_date + ", tournament=" + tournament + ", team1=" + team1 + ", team2=" + team2
				+ ", result1=" + result1 + ", result2=" + result2 + ", status=" + status + ", match_type=" + match_type
				+ ", winner=" + winner + ", match_name=" + match_name + "]";
	}

}
