package com.avoidgroup.esports.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONArray;
import org.json.JSONException;

public class PandaScoreSerieUtil {
	private static final String ACCESS_TOKEN = "sL3PReaZ8uJPnbKFbMrIEukZ7rOxpaEQ-xmPRwNkHl_sBfB9cLc";

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	private static JSONArray readJsonFromUrl(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			// JSONObject json = new JSONObject(jsonText);
			JSONArray values = new JSONArray(jsonText);
			return values;
		} finally {
			is.close();
		}
	}

	public JSONArray GetSeries() {

		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/series?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
	public JSONArray GetPastSeries() {

		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/series/past?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
	public JSONArray GetRunningSeries() {

		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/series/runing?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
	public JSONArray GetUpComingSeries() {

		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/series/upcoming?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
	public JSONArray GetASeries(String serieID) {

		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/series/"+serieID+"?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
	public JSONArray GetMatchesForASerie(String serieID) {

		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/series/"+serieID+"/matches?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
	public JSONArray GetPastMatchesForASeries(String serieID) {

		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/series/"+serieID+"/matches/past?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
	public JSONArray GetRunningMatchesForASerie(String serieID) {

		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/series/"+serieID+"/matches/running?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
	public JSONArray GetUpComingMatchesForASerie(String serieID) {

		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/series/"+serieID+"/matches/upcoming?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
	public JSONArray GetTournamentsForASerie(String serieID) {

		try {

			JSONArray values = readJsonFromUrl(
					"https://api.pandascore.co/series/"+serieID+"/tournaments?token="+ACCESS_TOKEN);

			return values;
		} catch (Exception e) {
			System.out.println("erro: " + e.getMessage());
			return null;
		}

	}
}
