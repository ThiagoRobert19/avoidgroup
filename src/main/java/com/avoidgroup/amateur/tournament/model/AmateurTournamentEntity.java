package com.avoidgroup.amateur.tournament.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.avoidgroup.team.model.TeamEntity;
import com.avoidgroup.tournaments.model.GameEntity;
import com.avoidgroup.util.EntidadeBase;

@Component("AmateurTournamentEntity")
@Entity
public class AmateurTournamentEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;
	private String uuid;

	private Integer lineUpCount;
	private String matchstatus;
	private String description;
	private Integer max_allowed;
	private Integer numberOfGame;
	@Temporal(TemporalType.DATE)
	private Date dateOfPublication;

	@Temporal(TemporalType.DATE)
	private Date begin_date;

	private Integer begin_hour;
	private Integer end_hour;
	
	private String ampm;

	@Temporal(TemporalType.DATE)
	private Date end_date;
	@OneToOne
	@JoinColumn(name = "game_id")
	private GameEntity game;

	private String image_default;

	private String image_default_name;

	@OneToOne
	@JoinColumn(name = "winner_id")
	private TeamEntity winner;

	@OneToOne
	@JoinColumn(name = "creator_id")
	private TeamEntity creator;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getLineUpCount() {
		return lineUpCount;
	}

	public void setLineUpCount(Integer lineUpCount) {
		this.lineUpCount = lineUpCount;
	}

	public String getMatchstatus() {
		return matchstatus;
	}

	public void setMatchstatus(String matchstatus) {
		this.matchstatus = matchstatus;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMax_allowed() {
		return max_allowed;
	}

	public void setMax_allowed(Integer max_allowed) {
		this.max_allowed = max_allowed;
	}

	public Integer getNumberOfGame() {
		return numberOfGame;
	}

	public void setNumberOfGame(Integer numberOfGame) {
		this.numberOfGame = numberOfGame;
	}

	public Date getDateOfPublication() {
		return dateOfPublication;
	}

	public void setDateOfPublication(Date dateOfPublication) {
		this.dateOfPublication = dateOfPublication;
	}

	public Date getBegin_date() {
		return begin_date;
	}

	public void setBegin_date(Date begin_date) {
		this.begin_date = begin_date;
	}

	public Integer getBegin_hour() {
		return begin_hour;
	}

	public void setBegin_hour(Integer begin_hour) {
		this.begin_hour = begin_hour;
	}

	public Integer getEnd_hour() {
		return end_hour;
	}

	public void setEnd_hour(Integer end_hour) {
		this.end_hour = end_hour;
	}

	public String getAmpm() {
		return ampm;
	}

	public void setAmpm(String ampm) {
		this.ampm = ampm;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public GameEntity getGame() {
		return game;
	}

	public void setGame(GameEntity game) {
		this.game = game;
	}

	public String getImage_default() {
		return image_default;
	}

	public void setImage_default(String image_default) {
		this.image_default = image_default;
	}

	public String getImage_default_name() {
		return image_default_name;
	}

	public void setImage_default_name(String image_default_name) {
		this.image_default_name = image_default_name;
	}

	public TeamEntity getWinner() {
		return winner;
	}

	public void setWinner(TeamEntity winner) {
		this.winner = winner;
	}

	public TeamEntity getCreator() {
		return creator;
	}

	public void setCreator(TeamEntity creator) {
		this.creator = creator;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "AmateurTournamentEntity [id=" + id + ", name=" + name + ", uuid=" + uuid + ", lineUpCount="
				+ lineUpCount + ", matchstatus=" + matchstatus + ", description=" + description + ", max_allowed="
				+ max_allowed + ", numberOfGame=" + numberOfGame + ", dateOfPublication=" + dateOfPublication
				+ ", begin_date=" + begin_date + ", begin_hour=" + begin_hour + ", end_hour=" + end_hour + ", ampm="
				+ ampm + ", end_date=" + end_date + ", game=" + game + ", image_default=" + image_default
				+ ", image_default_name=" + image_default_name + ", winner=" + winner + ", creator=" + creator + "]";
	}

	

}
