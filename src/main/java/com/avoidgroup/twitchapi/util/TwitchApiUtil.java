package com.avoidgroup.twitchapi.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.avoidgroup.esports.model.MatchOfTheDayEntity;
import com.avoidgroup.twitchapi.model.ProfileTwitchEntity;
import com.avoidgroup.twitchapi.model.TopGamesTwitchEntity;

public class TwitchApiUtil {
	private static String CLIENT_ID = "1afzrxcxqvcu1h3tok5mysmzar99ga";
	private static String CLIENT_SECRET = "t4oylvdtc64oom0yj1m804dyzt9ekk";

	private static String AUTH_PATH = "https://id.twitch.tv";

	public String getAccessToken(String code) {

		String access_token = "";

		HttpPost request = new HttpPost("https://id.twitch.tv/oauth2/token?client_id=" + CLIENT_ID + "&client_secret="
				+ CLIENT_SECRET + "&code=" + code
				+ "&grant_type=authorization_code&redirect_uri=http://localhost:8080/AvoidGroup/twitch/auth");

		HttpResponse response = null;
		try {
			HttpClient httpClient = new DefaultHttpClient();
			response = httpClient.execute(request);

			String json = EntityUtils.toString(response.getEntity(), "UTF-8");

			JSONObject obj = new JSONObject(json);

			access_token = obj.get("access_token").toString();

		} catch (ClientProtocolException e) {
			System.out.println("Erro1: " + e.getMessage());

		} catch (IOException e) {
			System.out.println("Erro2: " + e.getMessage());
		}
		return access_token;
	}

	public void revokeAccessToken(String token) {

		HttpPost request = new HttpPost(
				"https://id.twitch.tv/oauth2/revoke?client_id=" + CLIENT_ID + "&token=" + token);
		request.setHeader("client_id", CLIENT_ID);
		request.setHeader("Authorization", "Bearer " + token);
		HttpResponse response = null;
		try {
			HttpClient httpClient = new DefaultHttpClient();
			response = httpClient.execute(request);

		} catch (ClientProtocolException e) {
			System.out.println("Erro1: " + e.getMessage());

		} catch (IOException e) {
			System.out.println("Erro2: " + e.getMessage());
		}

	}

	public ProfileTwitchEntity getUser(String token) {

		ProfileTwitchEntity profileTwitch = new ProfileTwitchEntity();
		String userId = "";

		HttpGet request = new HttpGet("https://api.twitch.tv/helix/users");
		request.setHeader("client_id", CLIENT_ID);
		request.setHeader("Authorization", "Bearer " + token);
		HttpResponse response = null;
		try {
			HttpClient httpClient = new DefaultHttpClient();
			response = httpClient.execute(request);
			System.out.println("deu certo:");
			Header[] headers = response.getAllHeaders();
			for (Header header : headers) {
				System.out.println("Key : " + header.getName() + " | Value : " + header.getValue());

			}

			String json = EntityUtils.toString(response.getEntity(), "UTF-8");
			System.out.println(response.getStatusLine().getStatusCode());

			System.out.println(json);
			JSONObject obj = new JSONObject(json);
			JSONArray values = obj.getJSONArray("data");

			for (int i = 0; i < values.length(); i++) {

				JSONObject userJ = values.getJSONObject(i);
				userId = userJ.get("id").toString();
				profileTwitch.setTwitchID(userId);
				String userLogin = userJ.get("login").toString();
				profileTwitch.setChannelName(userLogin);
			}

		} catch (ClientProtocolException e) {
			System.out.println("Erro1: " + e.getMessage());

		} catch (IOException e) {
			System.out.println("Erro2: " + e.getMessage());
		}
		return profileTwitch;
	}

	public List<TopGamesTwitchEntity> getTopGames() {
		List<TopGamesTwitchEntity> listTopGames = new ArrayList<TopGamesTwitchEntity>();

		HttpGet request = new HttpGet(
				"https://api.twitch.tv/helix/games/top?client_id=" + CLIENT_ID + "&limit=10&offset=1");
		request.setHeader("Client-ID", CLIENT_ID);
		HttpResponse response = null;
		try {
			HttpClient httpClient = new DefaultHttpClient();
			response = httpClient.execute(request);

			String json = EntityUtils.toString(response.getEntity(), "UTF-8");
			JSONObject obj = new JSONObject(json);
			JSONArray values = obj.getJSONArray("data");

			for (int i = 0; i < values.length(); i++) {
				TopGamesTwitchEntity topGames = new TopGamesTwitchEntity();
				JSONObject topG = values.getJSONObject(i);

				topGames.setGame_id(topG.get("id").toString());

				String art = topG.get("box_art_url").toString();
				art = art.replace("{width}", "300").replace("{height}", "300");
				topGames.setGame_img(art);

				topGames.setGame_name(topG.get("name").toString());
				topGames.setGame_link("https://www.twitch.tv/directory/game/"+topGames.getGame_name()+"");

				listTopGames.add(topGames);
			}

		} catch (ClientProtocolException e) {
			System.out.println("Erro1: " + e.getMessage());

		} catch (IOException e) {
			System.out.println("Erro2: " + e.getMessage());
		}
		return listTopGames;
	}

	public String getStream() {
		String json = "";
		
		HttpGet request = new HttpGet("https://api.twitch.tv/helix/streams?first=1");
		request.setHeader("Client-ID", CLIENT_ID);
		HttpResponse response = null;
		try {
			HttpClient httpClient = new DefaultHttpClient();
			response = httpClient.execute(request);

			json = EntityUtils.toString(response.getEntity(), "UTF-8");

		} catch (ClientProtocolException e) {
			System.out.println("Erro1: " + e.getMessage());

		} catch (IOException e) {
			System.out.println("Erro2: " + e.getMessage());
		}
		return json;
	}

}
