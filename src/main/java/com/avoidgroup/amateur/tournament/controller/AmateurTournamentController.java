package com.avoidgroup.amateur.tournament.controller;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.avoidgroup.amateur.tournament.model.AmateurLineUpEntity;
import com.avoidgroup.amateur.tournament.model.AmateurMatchEntity;
import com.avoidgroup.amateur.tournament.model.AmateurTournamentEntity;
import com.avoidgroup.amateur.tournament.model.AmateurTournamentInvitationEntity;
import com.avoidgroup.dao.GenericDao;
import com.avoidgroup.model.FriendEntity;
import com.avoidgroup.model.ProfileEntity;
import com.avoidgroup.team.model.TeamEntity;
import com.avoidgroup.team.model.TeamProfileEntity;
import com.avoidgroup.tournaments.model.GameEntity;
import com.avoidgroup.util.Common;
import com.avoidgroup.util.DropBoxUtil;
import com.google.gson.Gson;

@Controller
@RequestMapping(value = "/amateurtournament")
public class AmateurTournamentController {
	@Autowired
	private GenericDao<AmateurTournamentEntity> daoTournament;
	@Autowired
	private List<AmateurTournamentEntity> listATournament;

	@Autowired
	private List<FriendEntity> listFriend;

	@Autowired
	private List<TeamEntity> listTeam;
	@Autowired
	private AmateurTournamentEntity tournament;
	@Autowired
	private AmateurLineUpEntity lineUp;
	@Autowired
	private List<AmateurLineUpEntity> listLineUp;

	@Autowired
	private GenericDao<AmateurLineUpEntity> daoLineUp;

	@Autowired
	private AmateurMatchEntity match;
	@Autowired
	private List<AmateurMatchEntity> listMatch;
	@Autowired
	private GenericDao<AmateurMatchEntity> daoMatch;

	@Autowired
	private ProfileEntity profile;

	@Autowired
	private GenericDao<TeamEntity> daoTeam;
	@Autowired
	private TeamEntity team;
	@Autowired
	private List<TeamProfileEntity> listTeamProfile;
	@Autowired
	private GenericDao<TeamProfileEntity> daoTeamProfile;
	@Autowired
	private List<AmateurTournamentInvitationEntity> listInvitationTournament;

	@Autowired
	private GenericDao<FriendEntity> daoFriend;

	@Autowired
	private AmateurTournamentInvitationEntity invitation;
	@Autowired
	private Common common;

	@Autowired
	private GenericDao<AmateurTournamentInvitationEntity> daoInvitation;

	@RequestMapping(value = "/viewMatches/{tourID}", method = RequestMethod.GET)
	public ModelAndView matches(@PathVariable(value = "tourID") String tourID, ModelAndView model,
			HttpServletRequest request) {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapMatch = new HashMap<String, Object>();
		mapMatch.put("tournament.id", Integer.parseInt(tourID));
		mapMatch.put("match_type", "ROUND 1");

		listMatch = daoMatch.listarProperty(AmateurMatchEntity.class, mapMatch, "and");

		String json = new Gson().toJson(listMatch);

		System.out.println("lista: " + listMatch.toString());
		model.addObject("json", json);
		model.setViewName("amateurtournament/round");
		return model;

	}

	@RequestMapping(value = "/generatematches/{tourID}", method = RequestMethod.GET)
	public ModelAndView generatematches(@PathVariable(value = "tourID") String tourID, ModelAndView model,
			HttpServletRequest request) throws ParseException {

		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapLine = new HashMap<String, Object>();
		mapLine.put("tournament.id", Integer.parseInt(tourID));

		if (daoLineUp.exist(AmateurLineUpEntity.class, mapLine, "and")) {

			if (!daoMatch.exist(AmateurMatchEntity.class, mapLine, "and")) {
				listLineUp = daoLineUp.listarProperty(AmateurLineUpEntity.class, mapLine, "and");
				Collections.shuffle(listLineUp);
				for (int tam = 0; tam < listLineUp.size(); tam++) {

					AmateurLineUpEntity lineA = new AmateurLineUpEntity();
					AmateurLineUpEntity lineB = new AmateurLineUpEntity();
					AmateurMatchEntity match = new AmateurMatchEntity();

					lineA = listLineUp.get(tam);

					lineB = listLineUp.get(tam + 1);
					match.setTeam1(lineA.getTeam());
					match.setTeam2(lineB.getTeam());

					match.setTournament(lineA.getTournament());
					Date begin_date = match.getTournament().getBegin_date();

					Date dateOfMatch = common.RandomDateRound(begin_date);

					Date timeOfMatch = common.RandomTime(lineA.getTournament().getBegin_hour(),
							lineA.getTournament().getEnd_hour(), lineA.getTournament().getAmpm());

					match.setBegin_date(dateOfMatch);
					match.setBegin_hour(timeOfMatch);
					match.setMatch_type("ROUND 1");
					match.setStatus("TBA");
					switch (tam) {
					case 0:
						match.setMatch_name("A");
						break;
					case 2:
						match.setMatch_name("B");
						break;
					case 4:
						match.setMatch_name("C");
						break;
					case 6:
						match.setMatch_name("D");
						break;
					case 8:
						match.setMatch_name("E");
						break;

					}

					daoMatch.saveUpdate(match);

					tam++;

				}
				model.setViewName("redirect:/amateurtournament/manage/" + tourID);
				return model;
			}
			// MUDAR AQUI
			model.setViewName("redirect:/");
			return model;
		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/lineupremove/{lineID}", method = RequestMethod.GET)
	public ModelAndView lineupremove(@PathVariable(value = "lineID") String lineID, ModelAndView model,
			HttpServletRequest request) {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapLine = new HashMap<String, Object>();
		mapLine.put("id", Integer.parseInt(lineID));

		if (daoLineUp.exist(AmateurLineUpEntity.class, mapLine, "and")) {
			lineUp = daoLineUp.buscaId(AmateurLineUpEntity.class, Integer.parseInt(lineID));
			String tourID = lineUp.getTournament().getId().toString();
			if (lineUp.getTournament().getCreator().getOwner().getId().equals(profile.getId())) {

				daoLineUp.remove(AmateurLineUpEntity.class, Integer.parseInt(lineID));
				model.setViewName("redirect:/amateurtournament/manage/" + tourID);
				return model;

			} else {
				model.setViewName("redirect:/amateurtournament/manage/" + tourID);
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/sample", method = RequestMethod.GET)
	public ModelAndView sample(ModelAndView model) {
		model.setViewName("amateurtournament/sample");
		return model;

	}

	@RequestMapping(value = "/lineup/{tourID}", method = RequestMethod.GET)
	public ModelAndView lineup(@PathVariable(value = "tourID") String tourID, ModelAndView model,
			HttpServletRequest request) {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapTour = new HashMap<String, Object>();
		mapTour.put("id", Integer.parseInt(tourID));

		if (daoTournament.exist(AmateurTournamentEntity.class, mapTour, "and")) {
			tournament = daoTournament.buscaId(AmateurTournamentEntity.class, Integer.parseInt(tourID));
			Map<String, Object> mapLineup = new HashMap<String, Object>();

			mapLineup.put("tournament.id", Integer.parseInt(tourID));
			listLineUp = daoLineUp.listarProperty(AmateurLineUpEntity.class, mapLineup, "and");

			model.addObject("tournament", tournament);
			model.addObject("listLineUp", listLineUp);
			model.setViewName("amateurtournament/lineup");
			return model;

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/declinerequest/{requestID}", method = RequestMethod.GET)
	public ModelAndView declineRequest(@PathVariable(value = "requestID") String requestID, ModelAndView model,
			HttpServletRequest request) {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapInvitation = new HashMap<String, Object>();
		mapInvitation.put("id", Integer.parseInt(requestID));

		if (daoInvitation.exist(AmateurTournamentInvitationEntity.class, mapInvitation, "and")) {
			invitation = daoInvitation.buscaId(AmateurTournamentInvitationEntity.class, Integer.parseInt(requestID));

			if (invitation.getInvited().getOwner().getId().equals(profile.getId())) {
				String teamID = invitation.getInvited().getId().toString();
				daoInvitation.delete(AmateurTournamentInvitationEntity.class, mapInvitation, "and");

				model.setViewName("redirect:/team/viewTeam/" + teamID);
				return model;

			} else {
				model.setViewName("redirect:/team/viewTeam/" + invitation.getInvited().getId());
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/acceptrequest/{requestID}", method = RequestMethod.GET)
	public ModelAndView acceptRequest(@PathVariable(value = "requestID") String requestID, ModelAndView model,
			HttpServletRequest request) {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapInvitation = new HashMap<String, Object>();
		mapInvitation.put("id", Integer.parseInt(requestID));

		if (daoInvitation.exist(AmateurTournamentInvitationEntity.class, mapInvitation, "and")) {
			invitation = daoInvitation.buscaId(AmateurTournamentInvitationEntity.class, Integer.parseInt(requestID));

			if (invitation.getInvited().getOwner().getId().equals(profile.getId())) {
				AmateurTournamentEntity t = new AmateurTournamentEntity();
				t = invitation.getTournament();
				AmateurLineUpEntity line = new AmateurLineUpEntity();
				line.setTournament(t);
				line.setTeam(invitation.getInvited());
				daoLineUp.saveUpdate(line);

				daoInvitation.delete(AmateurTournamentInvitationEntity.class, mapInvitation, "and");

				model.setViewName("redirect:/team/viewTeam/" + invitation.getInvited().getId());
				return model;

			} else {
				model.setViewName("redirect:/team/viewTeam/" + invitation.getInvited().getId());
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/viewyourrequests/{teamID}", method = RequestMethod.GET)
	public ModelAndView viewyourrequest(@PathVariable(value = "teamID") String teamID, ModelAndView model,
			HttpServletRequest request) {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapTeam = new HashMap<String, Object>();
		mapTeam.put("id", Integer.parseInt(teamID));

		if (daoTeam.exist(TeamEntity.class, mapTeam, "and")) {
			team = daoTeam.buscaId(TeamEntity.class, Integer.parseInt(teamID));
			if (team.getOwner().getId().equals(profile.getId())) {

				Map<String, Object> map = new HashMap<String, Object>();
				map.put("invited.id", Integer.parseInt(teamID));
				map.put("status", "requested");

				listInvitationTournament = daoInvitation.listarProperty(AmateurTournamentInvitationEntity.class, map,
						"and");
				model.addObject("listInvitationTournament", listInvitationTournament);
				model.setViewName("amateurtournament/viewyourrequests");
				return model;

			} else {
				model.setViewName("redirect:/team/viewTeam/" + teamID);
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/sendrequest/{teamID}/{tourID}", method = RequestMethod.GET)
	public ModelAndView sendRequest(@PathVariable(value = "teamID") String teamID,
			@PathVariable(value = "tourID") String tourID, ModelAndView model, HttpServletRequest request) {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapTour = new HashMap<String, Object>();
		mapTour.put("id", Integer.parseInt(tourID));

		if (daoTournament.exist(AmateurTournamentEntity.class, mapTour, "and")) {
			tournament = daoTournament.buscaId(AmateurTournamentEntity.class, Integer.parseInt(tourID));
			if (tournament.getCreator().getOwner().getId().equals(profile.getId())) {

				Map<String, Object> map = new HashMap<String, Object>();
				map.put("invited.id", Integer.parseInt(teamID));
				map.put("tournament.id", Integer.parseInt(tourID));
				map.put("status", "requested");

				if (!daoInvitation.exist(AmateurTournamentInvitationEntity.class, map, "and")) {
					team = daoTeam.buscaId(TeamEntity.class, Integer.parseInt(teamID));
					Calendar cal = Calendar.getInstance();
					AmateurTournamentInvitationEntity inv = new AmateurTournamentInvitationEntity();
					inv.setDateOfInvitation(cal.getTime());
					inv.setInvited(team);
					inv.setStatus("requested");
					inv.setTournament(tournament);
					daoInvitation.saveUpdate(inv);
					model.setViewName("redirect:/amateurtournament/createrequest/" + tournament.getId() + "");
					return model;
				} else {
					model.setViewName("redirect:/amateurtournament/createrequest/" + tournament.getId() + "");
					return model;
				}

			} else {
				model.setViewName("redirect:/amateurtournament/manage/" + tournament.getId());
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/cancelrequest/{teamID}/{tourID}", method = RequestMethod.GET)
	public ModelAndView cancelRequest(@PathVariable(value = "teamID") String teamID,
			@PathVariable(value = "tourID") String tourID, ModelAndView model, HttpServletRequest request) {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapTour = new HashMap<String, Object>();
		mapTour.put("id", Integer.parseInt(tourID));

		if (daoTournament.exist(AmateurTournamentEntity.class, mapTour, "and")) {
			tournament = daoTournament.buscaId(AmateurTournamentEntity.class, Integer.parseInt(tourID));
			if (tournament.getCreator().getOwner().getId().equals(profile.getId())) {

				Map<String, Object> map = new HashMap<String, Object>();
				map.put("invited.id", Integer.parseInt(teamID));
				map.put("tournament.id", Integer.parseInt(tourID));
				map.put("status", "requested");
				daoInvitation.delete(AmateurTournamentInvitationEntity.class, map, "and");

				model.setViewName("redirect:/amateurtournament/createrequest/" + tourID);
				return model;
			} else {
				model.setViewName("redirect:/team/viewTeam/" + tournament.getCreator().getId() + "");
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/createrequest/{tourID}", method = RequestMethod.GET)
	public ModelAndView request(@PathVariable(value = "tourID") String tourID, ModelAndView model,
			HttpServletRequest request) {

		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapTour = new HashMap<String, Object>();
		mapTour.put("id", Integer.parseInt(tourID));

		if (daoTournament.exist(AmateurTournamentEntity.class, mapTour, "and")) {
			tournament = daoTournament.buscaId(AmateurTournamentEntity.class, Integer.parseInt(tourID));

			if (tournament.getCreator().getOwner().getId().equals(profile.getId())) {

				Map<String, Object> mapLineup = new HashMap<String, Object>();
				mapLineup.put("tournament.id", Integer.parseInt(tourID));

				Integer line = daoLineUp.count("AmateurLineUpEntity", "tournament.id", tourID);
				if (line >= tournament.getMax_allowed()) {
					model.setViewName("redirect:/amateurtournament/lineup/" + tourID);
					return model;
				} else {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("profile1.id", profile.getId());
					map.put("profile2.id", profile.getId());

					listFriend = daoFriend.listarProperty(FriendEntity.class, map, "or");

					for (FriendEntity f : listFriend) {
						Map<String, Object> mapTeamProfile = new HashMap<String, Object>();
						mapTeamProfile.put("profile.id", f.getId());

						listTeamProfile = daoTeamProfile.listarProperty(TeamProfileEntity.class, mapTeamProfile, "and");

					}
					Map<String, Object> maptpa = new HashMap<String, Object>();
					maptpa.put("profile.id", profile.getId());

					listTeamProfile = daoTeamProfile.listarProperty(TeamProfileEntity.class, maptpa, "and");

					List<TeamProfileEntity> listTP = new ArrayList<TeamProfileEntity>();

					for (TeamProfileEntity tp : listTeamProfile) {

						Map<String, Object> mapReq = new HashMap<String, Object>();
						mapReq.put("invited.id", tp.getTeam().getId());
						mapReq.put("tournament.id", tournament.getId());
						mapReq.put("status", "requested");

						if (daoInvitation.exist(AmateurTournamentInvitationEntity.class, mapReq, "and")) {
							TeamEntity t = tp.getTeam();
							t.setRequestTournament("yes");
							tp.setTeam(t);
						}
						if (!tp.getTeam().getId().equals(tournament.getCreator().getId())) {
							listTP.add(tp);
						}
					}
					Integer countRequest = daoInvitation.count("AmateurTournamentInvitationEntity", "tournament.id",
							tourID);
					model.addObject("lista", listTP);
					model.addObject("line", line);
					model.addObject("countRequest", countRequest);
					model.addObject("tournament", tournament);
					model.setViewName("amateurtournament/createrequest");

					return model;
				}

			} else {

				model.setViewName("redirect:/team/viewTeam/" + team.getId().toString());
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/manage/{tourID}", method = RequestMethod.GET)
	public ModelAndView manage(@PathVariable(value = "tourID") String tourID, ModelAndView model,
			HttpServletRequest request) {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapTour = new HashMap<String, Object>();
		mapTour.put("id", Integer.parseInt(tourID));

		if (daoTournament.exist(AmateurTournamentEntity.class, mapTour, "and")) {
			Map<String, Object> mapMatch = new HashMap<String, Object>();
			mapMatch.put("tournament.id", Integer.parseInt(tourID));
			if (daoMatch.exist(AmateurMatchEntity.class, mapMatch, "and")) {
				model.addObject("matchsetup", "yes");

			} else {
				model.addObject("matchsetup", "no");
			}

			tournament = daoTournament.buscaId(AmateurTournamentEntity.class, Integer.parseInt(tourID));
			Integer line = daoLineUp.count("AmateurLineUpEntity", "tournament.id", tourID);
			model.addObject("line", line);
			model.addObject("tournament", tournament);
			model.setViewName("amateurtournament/manage");
			return model;

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/viewbyteam/{teamID}", method = RequestMethod.GET)
	public ModelAndView view(@PathVariable(value = "teamID") String teamID, ModelAndView model,
			HttpServletRequest request) {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapTeam = new HashMap<String, Object>();
		mapTeam.put("id", Integer.parseInt(teamID));

		if (daoTeam.exist(TeamEntity.class, mapTeam, "and")) {
			team = daoTeam.buscaId(TeamEntity.class, Integer.parseInt(teamID));
			if (team.getOwner().getId().equals(profile.getId())) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("creator.id", Integer.parseInt(teamID));

				listATournament = daoTournament.listarProperty(AmateurTournamentEntity.class, map, "and");
				Map<String, Object> mapLineUp = new HashMap<String, Object>();
				mapLineUp.put("team.id", Integer.parseInt(teamID));

				listLineUp = daoLineUp.listarProperty(AmateurLineUpEntity.class, mapLineUp, "and");
				model.addObject("listATournament", listATournament);
				model.addObject("listLineUp", listLineUp);
				model.addObject("team", team);
				model.setViewName("amateurtournament/viewbyteam");
				return model;

			} else {

				model.setViewName("redirect:/team/viewTeam/" + team.getId().toString());
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/add/{teamID}", method = RequestMethod.GET)
	public ModelAndView create(@PathVariable(value = "teamID") String teamID, ModelAndView model,
			HttpServletRequest request) {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapTeam = new HashMap<String, Object>();
		mapTeam.put("id", Integer.parseInt(teamID));

		if (daoTeam.exist(TeamEntity.class, mapTeam, "and")) {
			team = daoTeam.buscaId(TeamEntity.class, Integer.parseInt(teamID));
			if (team.getOwner().getId().equals(profile.getId())) {
				model.addObject("team", team);
				model.setViewName("amateurtournament/add");
				return model;

			} else {

				model.setViewName("redirect:/team/viewTeam/" + team.getId().toString());
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView add(HttpServletRequest request, AmateurTournamentEntity amateurTournament, String teamID,
			ModelAndView model, MultipartFile file, String begindate, String enddate, Integer beginhour,
			Integer endhour, String ampm) throws IllegalStateException, IOException, ParseException {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		team = daoTeam.buscaId(TeamEntity.class, Integer.parseInt(teamID));
		if (team.getOwner().getId().equals(profile.getId())) {
			Calendar date1 = Calendar.getInstance();
			amateurTournament.setDateOfPublication(date1.getTime());
			amateurTournament.setCreator(team);
			amateurTournament.setGame(team.getGame());
			amateurTournament.setMatchstatus("undefined");

			SimpleDateFormat begin_end = new SimpleDateFormat("yyyy-MM-dd");

			SimpleDateFormat hour = new SimpleDateFormat("HH:mm");

			final Calendar calData = Calendar.getInstance();

			calData.setTime(begin_end.parse(begindate));
			amateurTournament.setBegin_date(calData.getTime());
			calData.setTime(begin_end.parse(enddate));
			amateurTournament.setEnd_date(calData.getTime());

			amateurTournament.setBegin_hour(beginhour);

			amateurTournament.setEnd_hour(endhour);
			amateurTournament.setAmpm(ampm);

			final Calendar cal = Calendar.getInstance();
			amateurTournament.setDateOfPublication(cal.getTime());

			Integer numGame = 0;
			Integer numFinal = 0;

			numGame = amateurTournament.getMax_allowed();

			for (int a = numGame; a > 0; a++) {

			}

			do {
				numGame = (numGame / 2);
				numFinal = numFinal + numGame;
			} while (numGame > 1);

			amateurTournament.setNumberOfGame(numFinal);

			String url;
			SimpleDateFormat df = new SimpleDateFormat("ddMMyyyyHHmmss");
			if (file.toString() != null && !file.getOriginalFilename().equals("")) {
				File convFile = new File(request.getRealPath("/img/") + file.getOriginalFilename());
				file.transferTo(convFile);

				String nome = df.format(cal.getTime()) + amateurTournament.getName().trim()
						+ amateurTournament.getCreator().getId().toString() + "amateurTournament";
				url = "";
				url = DropBoxUtil.uploadFile(convFile, "/" + nome.trim() + ".jpg");

				amateurTournament.setImage_default_name(nome);
				amateurTournament.setImage_default(url);

			} else {
				amateurTournament.setImage_default_name(null);
				amateurTournament.setImage_default(null);
			}
			String uuid = UUID.randomUUID().toString().replace("-", "");
			amateurTournament.setUuid(uuid);
			daoTournament.saveUpdate(amateurTournament);

			Map<String, Object> mapUuid = new HashMap<String, Object>();
			mapUuid.put("uuid", uuid);

			tournament = daoTournament.findByProperty(AmateurTournamentEntity.class, mapUuid, "and");

			lineUp.setTournament(tournament);
			lineUp.setTeam(team);
			daoLineUp.saveUpdate(lineUp);

			model.setViewName("redirect:/team/viewTeam/" + team.getId().toString());
			return model;

		} else {

			model.setViewName("redirect:/team/viewTeam/" + team.getId().toString());
			return model;
		}

	}

	@RequestMapping(value = "/delete/{tourID}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable(value = "tourID") String tourID, ModelAndView model,
			HttpServletRequest request) {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");

		Map<String, Object> mapTour = new HashMap<String, Object>();
		mapTour.put("id", Integer.parseInt(tourID));
		String teamID = "";
		if (daoTournament.exist(AmateurTournamentEntity.class, mapTour, "and")) {
			tournament = daoTournament.buscaId(AmateurTournamentEntity.class, Integer.parseInt(tourID));
			teamID = tournament.getCreator().getId().toString();
			if (tournament.getCreator().getOwner().getId().equals(profile.getId())) {

				Map<String, Object> map = new HashMap<String, Object>();
				map.put("tournament.id", Integer.parseInt(tourID));

				daoInvitation.delete(AmateurTournamentInvitationEntity.class, map, "and");
				daoLineUp.delete(AmateurLineUpEntity.class, map, "and");
				daoMatch.delete(AmateurMatchEntity.class, map, "and");

				daoTournament.delete(AmateurTournamentEntity.class, mapTour, "and");

				model.setViewName("redirect:/amateurtournament/viewbyteam/" + teamID);
				return model;
			} else {
				model.setViewName("redirect:/team/viewTeam/" + teamID);
				return model;
			}

		} else {
			model.setViewName("redirect:/");
			return model;
		}

	}

	@RequestMapping(value = "/viewbyuser", method = RequestMethod.GET)
	public ModelAndView viewbyuser(ModelAndView model, HttpServletRequest request) {
		profile = (ProfileEntity) request.getSession().getAttribute("profileLogado");
		
		List<AmateurLineUpEntity> list = new ArrayList<AmateurLineUpEntity>();
		Map<String, Object> mapTeamProfile = new HashMap<String, Object>();
		mapTeamProfile.put("profile.id", profile.getId());

		listTeamProfile = daoTeamProfile.listarProperty(TeamProfileEntity.class, mapTeamProfile, "and");
		
		Map<String, Object> mapLineUp = new HashMap<String, Object>();
		Integer count = 1;
		for (TeamProfileEntity tp : listTeamProfile) {

			mapLineUp.put("team.id", tp.getTeam().getId());

			AmateurLineUpEntity amateurLine = new AmateurLineUpEntity();

			if (daoLineUp.exist(AmateurLineUpEntity.class, mapLineUp, "and")) {
				System.out.println("Passou: " + count);
				amateurLine = daoLineUp.findByProperty(AmateurLineUpEntity.class, mapLineUp, "or");
				list.add(amateurLine);
				count++;
			}

		}
		

		model.addObject("listLineUp", list);
		model.setViewName("amateurtournament/viewbyuser");
		return model;

	}
}
