package com.avoidgroup.tournaments.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.stereotype.Component;

import com.avoidgroup.util.EntidadeBase;

@Component("ProfessionalLeagueEntity")
@Entity
public class ProfessionalLeagueEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String name;

	private String description;

	private String image_default;

	private String image_default_name;

	public String getImage_default_name() {
		return image_default_name;
	}

	public void setImage_default_name(String image_default_name) {
		this.image_default_name = image_default_name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage_default() {
		return image_default;
	}

	public void setImage_default(String image_default) {
		this.image_default = image_default;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ProfessionalLeagueEntity [id=" + id + ", name=" + name + ", description=" + description
				+ ", image_default=" + image_default + ", image_default_name=" + image_default_name + "]";
	}

}
