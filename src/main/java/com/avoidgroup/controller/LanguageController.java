package com.avoidgroup.controller;

import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.avoidgroup.util.CulturaScope;

@Controller
@RequestMapping(value = "/language")
public class LanguageController {

	@Autowired
	private CulturaScope culturaScope;

	@RequestMapping(value = "/change/{lingua1}/{lingua2}", method = RequestMethod.GET)
	public ModelAndView change(@PathVariable(value = "lingua1") String lingua1,
			@PathVariable(value = "lingua2") String lingua2, HttpSession session, ModelAndView model) {
		Locale locale = new Locale(lingua1, lingua2);
		culturaScope.setLocale(locale);

		session.setAttribute("cultura", culturaScope);
		model.setViewName("redirect:/");
		return model;

	}

}
