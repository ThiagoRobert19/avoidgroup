package com.avoidgroup.steam.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.stereotype.Component;

import com.avoidgroup.util.EntidadeBase;

@Component("NewsForAppEntity")
@Entity
public class NewsForAppEntity implements Serializable, EntidadeBase {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String title;
	private String url;

	private String contents;
	private String feedlabel;
	private String date_new;
	private String image_url;
	private String game_name;
	private String game_appid;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getFeedlabel() {
		return feedlabel;
	}
	public void setFeedlabel(String feedlabel) {
		this.feedlabel = feedlabel;
	}
	public String getDate_new() {
		return date_new;
	}
	public void setDate_new(String date_new) {
		this.date_new = date_new;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public String getGame_name() {
		return game_name;
	}
	public void setGame_name(String game_name) {
		this.game_name = game_name;
	}
	public String getGame_appid() {
		return game_appid;
	}
	public void setGame_appid(String game_appid) {
		this.game_appid = game_appid;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "NewsForAppEntity [id=" + id + ", title=" + title + ", url=" + url + ", contents=" + contents
				+ ", feedlabel=" + feedlabel + ", date_new=" + date_new + ", image_url=" + image_url + ", game_name="
				+ game_name + ", game_appid=" + game_appid + "]";
	}

	

}
